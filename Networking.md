---
created: 2022-04-18T17:44:12-05:00
modified: 2022-04-19T19:05:18-05:00
---

# Networking and death

I noticed a ten years ago or so that about roughly one person who I knew reasonably well would die every single day ... EVERY SINGLE DAY, dammit! No breaks.

And it's not just people I know a little...I have lost a bunch of really, really close top-five closest best friends over the last 20 years ... 

... it's as if people who know me are trying to die. Sure, maybe you could say that they have their own reasons to die, but it seems like it's deliberate or something ... 🤔 

The number per day/week might vary a bit ... but more or less like clockwork, every month another 25-40 people were gone, ie more in Winter, fewer in Summer.

When I say that these are people who I have known "reasonably well" I mean someone who I got to know at least as well as anyone from 12 yrs of school ... so sorta well, in an adult sense... someone with whom I had had several meaningful conversations / mtgs / email exchanges / calls and other interactions that mattered ... usually even a couple arguments.

It seems like people just want to argue with me ... it's like they're arguing with me for sport or something ... 🤔 ... little do they know ...

I know almost no one from NW IA ... a few, but not many by comparison  ... at least, the number is small, only at most maybe 1000 or so ... most of the 25,000-40,000 people I know or have had meaningful interactions in my life are my adult life ... are from my post-Lyon County years ... after 1979

It is especially that way with people who I have gotten to know in the last ten years ... none of my business or interests are here ... and it's also a matter of how connections expand ... so most people are ties from my time at ISU in the SigEp fraternity around the country, from Ames City govt and politics, the Legislature, from  academia / research later, from my time as a Fed agent and then DoD contract "fixer", as a corporate employee or little  contract assignments, from some role in business, maybe start-ups, scientific research or philanthropic ventures.

Knowing a lot of people is one thing ... staying in touch is easier than it used to be ... I almost got used to a LOT of people  DYING ... but, actually, I didn't ... it always bugged me; still feels like a loss ... but it is just so routine now. 

I don't try to the obituaries any more; I sorta gave up ... there's always someone asking if I am going to make it to some funeral.


I decided that I was going to just avoid funerals and instead work  harder at meeting twenty-somethings or thirty somethings ... because life has to just fucking go on. 

The actuarial tables sorta scare me ... the ideal time to die is maybe five seconds from now, ie while one still feels pretty good ... EXCEPT if a guy stays alive for very long at all, he's going to need to add a lot more young friends ... the probability of living past 100 keeps going up and up.

https://www.ssa.gov/oact/STATS/table4c6.html
