---
created: 2022-04-01T08:31:12-05:00
modified: 2022-04-01T08:45:39-05:00
---

# Graphene membranes

https://youtu.be/ZqSgAsxXBUA

[Phys. Rev. E 102, 042101 (2020)  -  Fluctuation-induced current from freestanding graphene](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.102.042101)

[Connected Papers graph of Thibado paper on fluctuation-induced current from freestanding graphene](https://www.connectedpapers.com/main/48aeb087d9976753cc5314e290c52bbc665c94e3/Fluctuation%20induced-current-from-freestanding-graphene./graph) ... to ... [ConnectedPapers graph of Magnum paper on electrically conductive, highly flexible graphene membranes](https://www.connectedpapers.com/main/2012482892eb0ea139d546b764e24d0bb0f1c7f9/Mechanisms-of-Spontaneous-Curvature-Inversion-in-Compressed-Graphene-Ripples-for-Energy-Harvesting-Applications-via-Molecular-Dynamics-Simulations/graph)
