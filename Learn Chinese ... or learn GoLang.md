---
created: 2022-04-07T06:02:20-05:00
modified: 2022-04-07T06:10:24-05:00
---

# Learn Chinese ... or learn GoLang

Learning, failing, fixing failures, fighting frustration and working credibly in a new language confers a distinct neuroadvantage.

The people who do it are just flat out tougher than those who don't ... language training and mastery is matter of mental fitness ... it's never to late to start exercising.


https://twitter.com/DIFFversity/status/1512014525532459009
