---
created: 2022-03-25T09:41:27-05:00
modified: 2022-03-25T09:58:06-05:00
---

# Ryzen 5 5500U Mini-PC with 64 GB memory

[ASUS PN51 Mini PC with AMD Ryzen 5 5500U Six-Core Mobile Processor](https://www.newegg.com/asus-pn51-e1-bb5000xtd/p/N82E16856110213) with [64 gb memory](https://www.newegg.com/g-skill-64gb-260-pin-ddr4-so-dimm/p/N82E16820374025) .. no hard drive 

$461.99 + $199.99 + ??? = $661.98 + ???
