---
created: 2022-03-20T13:22:51-05:00
modified: 2022-03-20T13:25:06-05:00
---

# Watch

[Generics Go release 1.18](https://youtu.be/Pa_e9EeCdy8) and [GopherCon 2021](https://youtube.com/playlist?list=PL2ntRZ1ySWBfulCVQD6EaU8c-GM56aUU7)
