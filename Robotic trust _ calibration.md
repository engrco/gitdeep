---
created: 2022-03-29T12:28:27-05:00
modified: 2022-03-29T12:29:48-05:00
---

# Robotic trust / calibration

[Human-understandable competency self-assessments](https://arxiv.org/abs/2203.11981)
