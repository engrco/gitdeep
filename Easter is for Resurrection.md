---
created: 2022-04-16T23:08:57-05:00
modified: 2022-04-16T23:19:55-05:00
---

# Easter is for Resurrection

The elect are saved by grace, if they humble themselves and accept this grace.  That is why Easter is celebrated more than birthdays, more than Christmas or any other holiday.

Life on Earth is a blessing; no human born on this planet deserves even one second of breath ... if one cannot be grateful for this blessing, there's no foundation of hope for any lasting satisfaction in life.

It does not matter if you imagine that life is only a simulation...as long as you cannot be grateful for whatever the experience of being alive can possibly mean, you will suffer and deserve to suffer. 

You must be grateful to and respectful of your Creator ... regardless of whatever you might imagine the Creator to be ... or you will suffer more and be more ignorant than you need to be, ie you have to try to find and read the manual or understand whatever parts of it you can ....or you will suffer.
