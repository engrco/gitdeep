---
created: 2022-04-17T19:00:47-05:00
modified: 2022-04-17T19:07:01-05:00
---

# Why walk

Walking is better than a visit to Mayo ... better than 100 visits ... way better than being anywhere close to the ICU waiting-to-die processing center.

It's especially important to walk when someone [maybe yourself] is telling you that you're too sick to walk.

https://www.connectedpapers.com/main/fae8f8353c0631bd7547d4e1e9d57faedd9a0b5e/Short%20-and-Long%20term-exercise-induced-alterations-in-haemostasis%3A-a-review-of-the-literature./graph
