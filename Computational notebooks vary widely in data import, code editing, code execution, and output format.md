---
created: 2022-03-28T14:19:59-05:00
modified: 2022-03-28T14:21:57-05:00
---

# Computational notebooks vary widely in data import, code editing, code execution, and output format

[The Design Space of Computational Notebooks: An Analysis of 60 Systems in Academia and Industry.](https://www.samlau.me/pubs/computational-notebooks-design-space_VLHCC-2020.pdf)

Sam Lau, Ian Drosos, Julia M. Markel, Philip J. Guo. IEEE Symposium on Visual Languages and Human-Centric Computing (VL/HCC), 2020.
