---
title: Teach Yourself
subtitle: Dogfooding the dogfooded dogfood of dogfooding
comments: false
---

The person who learns the most in any classroom is the one who prepares the lesson and does the teaching ... if you want to learn anything in life, you will need to develop the proficiency in being profoundly and deeply excellent in teaching people, especially yourself.