---
title: About GitDeep
subtitle: Training gets deeper, Git never ends
comments: false
---

This is a study notebook ... for recursive, self-aware training focused on training the trainer ... dogfooding fully-instrumented code-ready repositories and workspaces for learning AI/ML, Git, data engineering, site reliability and value optimization of machine learning operations.