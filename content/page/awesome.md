---
title: Git your subconscious DEEP into the reading
subtitle: AWESOME reading lists
comments: false
---

We curate, discuss, debate, annotate AWESOME lists ... because learning is about failing a lot, with some pain ... and it's about IMMERSIVE reading so that ones has ideas about how to avoid most pain AND how to pick one's self up off the floor after one has fallen. 

Leaders are necessarily readers FIRST ... but reading alone is not really DEEP ... that's why we curate, discuss, debate, annotate, edit, revise, refactor, experiment with extreme ideas, break things, try to understand the specific mechanism of failure ... because AWESOME doesn't get to be AWESOME by forking something and giving it a new spin ... most things that people add to AWESOME lists are just list-filling things.