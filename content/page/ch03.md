---
title: Chapter 3
subtitle: Designing Data Pipelines
comments: false
---


## Overview of Data Pipelines

### Ingestion

### Storage

### Processing and Analysis

### Exploration and Visualization

## GCP Pipeline Components

## Migrating Hadoop and Spark to GCP

## Exam Essentials

## Review Questions
