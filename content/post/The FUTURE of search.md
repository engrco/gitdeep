---
created: 2022-01-31T13:21:33-06:00
lastmodified: 2022-01-31T13:30:51-06:00
---

# The FUTURE of search

Is always going to be about the database [which is sorta what the internet or fediverse is in an ideal sense] ... but it will also be about the physical linkage [even when that physical layer is something like electromagnetic radiation in the cosmos] to what is being searched for or probed ... you simply cannot find everything through the internet, but the internet can certainly inform your search in the physical realm.

It is not obvious how a token really helps with findability ... a token could help in being able to trust the idea you found EXCEPT that's dangerous because maybe we should never trust a damned token to be secure.
