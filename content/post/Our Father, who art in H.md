---
created: 2022-01-19T14:15:45-06:00
lastmodified: 2022-01-19T14:31:30-06:00
---

# Our Father, who art in Heaven

The sum total of everything, ALL of the driving forces behind the origin of ALL real and potential multiverses ... but EVERYTHING is your Creator...it may be more convenient if you simply DEFINE that sum total of everything that brought you into your existence as GOD ... then again if you're one of those traumatized hypochondriac who is triggered by some word or another, then using that DEFINITION is out for you.

It's very important to work with OBJECTIVE DEFINITIONS ... rather than to get hung up right away on FAITH ... for example, you might reject the word FATHER to describe your relationship with this thing you have defined as God ... it might work better for you to use the word Mother ... or to avoid any anthropomorphic language with non-gender word like Creator.

It's really YOUR choice ... but if you care at all about other humans, especially ancient humans who have been dead for hundreds or thousands of years you, you really  have think in terms of their language ... you can certainly demand to use your terminology, expressions and personal semiotics when you are expressing yourself ... but you cannot DEMAND that others use your terms ... NOBODY pays any attention to crybabies who demand to scream in their own personal language.

You can be ignored...but it is really your choice.
