---
created: 2022-02-09T04:44:42-06:00
lastmodified: 2022-02-09T04:49:46-06:00
---

# History is primarily about the study of failure and decline

We study past mistakes in order to NOT REPEAT THEM.

Excessive adoration, worship or reverence for the past is both pathology and pathetic ... what succeeded in the past was the ability to RUTHLESSLY look forward and to shamelessly move on to exploit new opportunities ... rather than paying any reverence to ancestor worship.
