---
created: 2022-02-17T12:58:14-06:00
lastmodified: 2022-02-17T13:05:05-06:00
---

# Deep communities require asynchronous workflows

In order to Git deep streams on human interaction from a global community of developers ... it's necessary to truly transcend the managerial thinking about workflows to string continuous integration / continuous development across deep intertwined layers of communities made up individual living all over the globe ... at some point in the future, location could stretch into space ... the central point is that Git must be used to effect efficient handoffs between responsible team members.
