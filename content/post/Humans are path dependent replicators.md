---
created: 2021-12-02T06:36:42-06:00
lastmodified: 2021-12-02T06:40:04-06:00
---

# Humans are path dependent replicators

Organizations, cultures, movements don't re-invent themselves...they use the old components, languages,semiotics that have worked...leadership is about guidance of the continuous improvement process ... it's NOT about ego.
