---
created: 2021-11-13T03:57:58-06:00
modified: 2021-11-13T04:33:18-06:00
---

# There's plenty of room at the bottom

In some sense, there's nothing new under the sun ... but while it is true that most of the wisdom of the ancients is still applicable to us today, we live in a world with spectacularly more instrumentation and data acquisition which provide data to us that is available in a non-rivalrous manner that could be, but often isn't exclusive ... and beyond just data, we have access to papers with code and technical papers which should not be [but often are] excludeble ... information and knowledge should be free, uncensored, non-excludable ... 

The excluders are out there ... but the overwhelming truth is that their is more information available at our fingertips than was not really even close to fathomable just a short time ago ... in other words, it's not just about the raw data or the sensors that allow us to see things in particle/plasma physics or the nanosphere of genetic code and bioprocesses or into deep space ... or even the truly immense processing power available for individuals to use to analyze all of that data ... it's about the vast communities of extremely intelligent people who do research, analyze data and write about their research ... our access to huge networked COMMUNITIES of spectacularly brilliant people in very narrow realms of the information realm is completely unprecedented.

To the extent that we are "stuck' in the physical realm OR worse completely distracted by idiotic arguments in politics, religion, sports or psychology (ie, emotions or hurt feelings), we are completely blind about what is happening in the vast realms of COMMUNITIES of scientists, artists, thinkers who populate the virtual realm.

It's really about the most efficient, effective, broadest, deepest consumption of data AND transformation of that data into knowledge as well as rumination on that knowledge into wisdom ... it's not that we should overthrow or reject the OLD wisdom of the ancients or those who have gone before us, it's about the NEW wisdom that we can find by exploring the brand new realms of the virtual noosphere.

Now is not the time to be STUCK in the physical realm ... it was NEVER EVER EVER  the time to be stuck in the pathetic emotional realm of gossip or arguments about hurt feelings.
