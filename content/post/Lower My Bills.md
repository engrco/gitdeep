---
created: 2021-11-05T05:23:48-05:00
modified: 2021-11-05T05:27:33-05:00
---

# Lower My Bills

Spend less ... spend 5% less every year, indefinitely.

Pareto your costs ... what costs 80% of your budget returns 20% of value ... addictions, conveniences, stuff that everyone else has/does do NOT add value ... understand exactly what adds value.
