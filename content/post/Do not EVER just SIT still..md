---
created: 2022-01-06T10:41:23-06:00
lastmodified: 2022-01-06T10:52:25-06:00
---

# Do not EVER just SIT still.

NEVER tolerate sitting as the norm; your skeleton, muscles and especially your internal organs work best and generally keep you at your fittest when you are standing and moving.

Guess what ... your brain works best sustainably when those things are healthy and fit. So if you want to stand out ... you have to be the one standing.

Sit only when you do not have any other choice ... you should see sitting as an enforced cultural maladaptation to classrooms in which the content was at best not disastrously terrible.

As a creative person, you must minimize the amount of time when your freedom to choose is taken from you.

https://twitter.com/QualityFellow/status/1479130292690776065
