---
title: Pleasant Walk
subtitle: It's What You Feel Entitled To
date: 2022-01-01
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

Embrace the suck!

Love everything about what you have been given ... if it's a cold, nasty day and you don't particularly like cold, nasty days EMBRACE IT and EMBRACE getting acclimated to cold, nasty days ... it will get colder.