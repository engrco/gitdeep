---
created: 2021-11-09T07:43:18-06:00
modified: 2021-11-09T09:55:20-06:00
---

# Successive Approximations and Refinements

... generally depend upon starting with something of strength or value ... but the appropriations and refinements themselves produce clarity.

How do we approximate?  Approximation itself is worthy of refinement and continually approximated improvement.

http://arxiv.org/abs/2111.03794
