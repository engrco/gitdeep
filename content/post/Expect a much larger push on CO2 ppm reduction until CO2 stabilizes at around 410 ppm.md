---
created: 2022-01-23T17:55:00-06:00
lastmodified: 2022-01-26T12:00:39-06:00
---

# Expect a much larger push on CO2 ppm reduction until CO2 stabilizes at around 410 ppm

https://pubs.geoscienceworld.org/gsa/geology/article/48/9/888/586769/A-23-m-y-record-of-low-atmospheric-CO2

https://gml.noaa.gov/ccgg/carbontracker/

Carbon should be focused in the organic matter on the surface ... mostly, that's about soil building ... but we also should get out of the way of more life, ie carbon sinks, in the oceans.

I am quite certain that if we knocked off the STUPIDITY of over-investing in the non-producing idiocy of "our renewable energy future" and the EXTRA STUPID lithium ion battery bandaids which are necessary to make unstable "renewable" energy work ... instead, we should embrace remote work more aggressively [to completely eliminate dependence on automotive culture and parking lots/streets as concrete solar collectors] ... harness  geothermal energy for controllable, on-demand steam turbines [and began understanding the Earth's core rather than just its skin] ... and over time, began to move new dwellings deep underneath the surface and so that we eliminated most [but not all] need for maintenance, upkeep insurance, heating/cooling or HVAC demand while turning roofs into quasi-enclosed patios, greenhouses,  conservatories or park-like edible landscaping.

https://gml.noaa.gov/ccgg/trends/gr.html
