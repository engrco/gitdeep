---
created: 2022-01-26T12:15:38-06:00
lastmodified: 2022-01-26T12:18:22-06:00
---

# What is Brilliant about?  Can it be done better with AWESOME lists or are they complements to one another?

https://brilliant.org/premium/

Comes down to time management ... Brilliant might be good for those times when one needs a temporary diversion.
