---
created: 2021-11-25T22:39:35-06:00
lastmodified: 2021-11-25T22:47:14-06:00
---

# Creativity demands lack of predictability

Normal people cannot STAND truly creative people...because true creativity demands NOT GETTING STUCK thinking like a predictable bricklayer ... it demands making leaps that a machine or artificial intelligence could make far better than a human being.

Normal people are correct in their fears of being replaced by automation or machine learning ...that necessarily will happen.

But there's still a NEED for creativity ... we just cannot ever expect it to be well-behaved, constrained or predictable.

Creative people will experience mood swings ... sleeping for what almost seems all day for months...and then sleeping 2hrs a day ... the difference in moods are necessary to inhabit or fully live in different psychological realms.
