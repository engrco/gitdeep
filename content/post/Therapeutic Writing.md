---
created: 2021-12-29T22:59:12-06:00
lastmodified: 2021-12-29T23:07:58-06:00
---

# Therapeutic Writing, Editing and Revision

I write, not for the catharsis, per se ... but more to share something with someone who might NEED to read it ... and I often find that as I live and change and grow, I eventually become that someone ... so it's not about me tomorrow or next week, but quite a bit later on ... when I have stopped feeling the initial fervor or passion that originally was much of what was driving me to write something
