---
created: 2021-11-20T19:02:45-06:00
lastmodified: 2021-11-20T19:09:55-06:00
---

# Use the BETTER side

Ever notice how some things are not appreciated AT ALL when they are out-of-season, after hours or, most of the time, just too obvious and taken completely for granted?

Why can't cities cultivate more valuable tree species, use trained arborist to trim/prune trees optimally [to keep City employees working on productive, beautifying things] and then use the wood/mulch for mushroom species ... allowing citizens to grow fresh mushrooms OR, if not enough citizens wanted to do that, selling mushroom kits to people who do want those kits?
