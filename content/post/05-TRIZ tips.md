---
title: TRIZ.tips
subtitle: Be more inventtive
date: 2021-01-01
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# [Invent the future of your species](https://twitter.com/QualityFellow/status/1475842689425981450)

FOCUS upon improving the quality of the distant future ... most of the stuff that humans do might actually destroy life or mindlessly ruin it, because humans are not paying attention to something magical that our Creator has provided.

TRIZ.tips is primarily about paying closer attention to ideas, ideation and invention ... especially, paying more attention to those ideas that can deliver value for the next year, decade, century, millenium and ten thousand years ... stop being mindless or doing anything that encourages people to be distracted or seek drunkenness or numbness.

Invent a more inventive atmosphere by studying invention rather than seeking distraction or diversion or advertisement.