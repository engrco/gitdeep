---
created: 2022-02-15T07:56:23-06:00
lastmodified: 2022-02-15T09:07:58-06:00
---

# I am not misanthropic ... I only hate certain behaviors in myself

taking property from others.ose behaviors ... maybe not hard enough, but I work at it and I work at being an example ... and I try not to give any oxygen or even resentment to the times when I am a bad example or encountered bad examples in others ... I TRY to forgive ... but I trying is not sufficient; I pray that I can forgive.

I HATE the behavior of ever imagining that someone else or something else is responsible for making me suffer ... I  HATE blaming others; I HATE ever adopting a victim mentality ... the blaming of others is a behavior DEEPLY DISGUSTS me ... I refuse to spend much time with people who do it because it is a behavior that rubs off and I don't want to be associated with any actions that further it.

I  HATE passively watching films, video or television...another behavior that DEEPLY DISGUSTS me ... I meditate, pray, contemplate and read books/papers and work through mathematic prrofs and concepts ... but I control the projector and I HATE ever ceding control of what goes into my mind and I am not at all sympathetic to any arguments for changing that ... passively consuming content is a weakness that I detest.

I detest gossip to such a degree that I strongly dislike social settings ... gossiping seems to almost be the one behavior that defines the most pathogenic, pathetic, utterly weak and disgusting trait of human beings ... the species deserves extinction for just one trait because it leads to envy and jealousy and all kinds of despicable activities such as lying, taking property from others or even violence. Gossips should just die ... short of that, they should be avoided at all costs.
