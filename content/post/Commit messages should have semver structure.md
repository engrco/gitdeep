---
created: 2022-02-18T06:00:10-06:00
lastmodified: 2022-02-18T06:13:18-06:00
---

# Commit messages should have semver structure

NOT like this

https://twitter.com/sassy_commits/status/1494635024645296135

But DO FOLLOW the @Sassy_Commits twitbot ... if only to think about how we could build a rulebot that would doynk the MORONS on the melon

https://twitter.com/sassy_commits

The structure or philosophy behind the structure should be something analogous to the thinking behind Semantic Versioning

https://semver.org/

Perhaps the best working example would be CONVENTIONAL COMMITS ... although there is probably room for improvement in the general specification for SPECIFIC codebases, the Conventional Commits specification is not bad

https://www.conventionalcommits.org/en/v1.0.0-beta.2/#specification

It's the THOUGHT that counts ... not the formalism ... but what the formal structure enables for everyone that come afterward
