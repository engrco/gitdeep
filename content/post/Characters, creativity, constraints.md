---
created: 2021-12-05T21:39:04-06:00
lastmodified: 2021-12-05T21:43:31-06:00
---

# Characters, creativity, constraints

It's usually necessary to strip away luxuries or extra cleverness to get down to what is valuable, what truly matters... creativity comes from the sparsiity of nothing but character.
