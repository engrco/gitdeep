---
created: 2022-01-08T13:27:19-06:00
lastmodified: 2022-01-08T13:29:56-06:00
---

# Understand the larger framework

Facts and factoids don't matter ... any can manufacture facts, slogans and witty aphorisms ... what matters is the general picture, the larger view ... PERSPECTIVE
