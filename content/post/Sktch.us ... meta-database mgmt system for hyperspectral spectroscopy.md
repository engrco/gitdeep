---
created: 2022-01-30T10:25:46-06:00
lastmodified: 2022-01-30T10:41:18-06:00
---

# Sktch.us ... meta-database mgmt system for hyperspectral spectroscopy

It's sketchy and scripts first ... because it starts out being about playing with ideas first, not necessarily at scale ... in order to develop the database mgmt architecture into C, which is at microscale and to ultimately put the logic directly into silicon, ie like the TPUs, which is at macro scale.

Scripting the interferometery and sketching out ways to use/improve existing collection strategies ... using existing database technologies like Cassandra, Mongo, MyQSL even spreadsheet-ey AirTable tools.

SKTCH.US is ultimately deeply COLLABORATIVE ... that's what the US is about ... how can groups of people COLLABORATIVELY learn and explore with hyperspectral imaging and near IR spectroscopy 
... that involves setting up Jupyter notebooks for rapid prototyping of data analysis and sketching out the strategies we might use for deep learning
