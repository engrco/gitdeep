---
created: 2022-02-14T07:46:13-06:00
lastmodified: 2022-02-15T10:25:04-06:00
---

# FREE [or highly-affordable] educational CONTENT itself is better now than it ever was in university classrooms

In order to develop skills that gain traction and actually provide a return on investment, the following #threadapalooza becomes necessary:

1) More-than-one-is-ready-for, aggravation-intensive in addition to the task at hand, performance-measured, real consequence, skin-in-the-game EXPERIENCE 

2) extemporaneous public speaking TEACHING opportunities to present material, share understanding, debate insights and be exposed to public humiliation

3) opportunities to work at scale developing a globally-competitive, significantly larger worldview or PERSPECTIVE for the relevance of what one does

4) the ability to completely IGNORE all attention-whoring hypnosis and other attempts at distraction, diversion, entertainment or advertisement which are about siezing mindshare without compensation

5) practice in developing an open source success cookbook or employee handbook that makes operations entirely TRANSPARENT

6) an understanding of how to best automate the process of FILTERING, sorting, sifting, grinding, roasting, perturbing and using junk to discern truth ... this inevitably leads to some sort of moral imperative or outight activism against censorious evil

7) understanding why formal meetings and classrooms [including online/virtual classrooms] are basically junk ... which is fine because we fill the hopper with junk in order to grind/blend/liquefy...but classrooms are the worst kind of junk when they are taken seriously ... NEVER SIT still for worthless junk; toy with it.

8) Watch a LOT less television or video ... when you need a diversion, explore the beauty of Mathematics ... it's easier now than it ever has been ... but it's still important to "dig your well before you are thirsty" https://en.wikipedia.org/wiki/Portal%3AMathematics

9) Follow the LIVES of those doing important work ... especially those who faced ridiculous obstacles, frustrations and distractions in the path to doing their work ... https://en.wikipedia.org/wiki/Lise_Meitner or the Humboldt Brothers ... https://en.wikipedia.org/wiki/Alexander_von_Humboldt or https://en.wikipedia.org/wiki/Wilhelm_von_Humboldt and the research university ... https://en.wikipedia.org/wiki/Humboldtian_model_of_higher_education
