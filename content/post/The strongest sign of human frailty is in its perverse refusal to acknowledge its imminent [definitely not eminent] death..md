---
created: 2022-01-31T03:30:22-06:00
lastmodified: 2022-01-31T04:06:12-06:00
---

# The strongest sign of human frailty is in its perverse refusal to acknowledge its imminent and [definitely not eminent] death.

Lives run their course and then cease ... death is like sleep ... except, of course we don't know anything about exactly how death is like sleep.

Death will come and when our death does come the entire Universe will be better off ... and that probably includes the soul of the being who is dying ... except that we cannot know that ... it's one of those things each one of us must come to terms with..

Death is not necessarily about waking again ... 
that's a nice, comforting story to sooth children but it's not really an adult story ... it's not really going to provide comfort to someone on their deathbed ... when you face imminent death you understand this..

The necessary thing to understand about death is about the necessity of love for one's Creator ... this goes along with the recognition that each moment of Life is precious and the similar recognition that death is part of the intelligence of the design of Life ... we have neither designed Life NOR have we any real understanding of death ... there's never been a funeral or celebration of life ceremony that comes close to helping us understand this CERTAINTY about our Creator.

Our Creator is not particularly fair or rational ... those are childish human notions ... our Creator is entirely about love and that love includes discipline and absolute law ... our love of our Creator provides the ONLY sustainable comfort or genuine wealth that we can possibly have ... this love must be greater than the love we have for ourselves, our capacities, our health, fitness or knowledge.  And those things must trump all other possessions or forms of wealth.

We can  only KNOW with absolute certainty that our Creator or whatever has brought us into Life ... is also whatever will ensure our demise.  Our Creator is far more intelligent than our species can ever hope to be with its entire collective understanding of the laws governing our Universe ... the myth that humans are created in the image of the Creator is a dangerous delusion ... if we indeed are an image of that Creator, we are at best like a ephemeral shadow on a foggy day ... all of humanity cannot fathom any feature of our Creator ... we can only KNOW that somehow we were brought into existence and and we should expect that living existence will cease.

If or whenever we love anything more than our Creator ....ANY thing includes ourselves, our species, all of our species "accomplishments" ... then we deserve an especially expeditious demise ... and we should expect that we will get what we deserve ... fortunately, our Creator is not about fairness.
