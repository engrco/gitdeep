---
created: 2021-12-16T19:17:43-06:00
lastmodified: 2021-12-18T19:25:56-06:00
---

# Exercise, USE, continual incremental improvement

Consider the case of Python ... C and C++ or even C# ... Ruby ... Go ... 

They are all great languages ... and each is PRACTICALLY indispensable for what each language does particularly ... but the demand for skills in a global sense tells us something about the populations of developers using and improving the languages ... while exercising one's skills/thinking in different languages is important for being a useful polyglot.

https://www.tiobe.com/tiobe-index/
