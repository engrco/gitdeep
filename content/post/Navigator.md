---
title: Integrate notetaker Google Keep with Git and NewsBlur
subtitle: Using Multiple Devices to Commit
date: 2021-11-04
tags: ["example", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# Navigator

The ORIGINAL ideas for the WWW were actually pretty good ... maybe BETTER than the tracking/analytics *webmassahs* now seem to need ... including Yahoo's approach to offering a directory of webpages.
