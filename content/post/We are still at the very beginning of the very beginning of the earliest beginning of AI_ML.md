---
created: 2022-01-30T18:25:26-06:00
lastmodified: 2022-01-30T18:27:40-06:00
---

# We are still at the very beginning of the very beginning of the earliest beginning of AI/ML

... but we might not survive to the part of the beginning where we are starting to get somewhere.
