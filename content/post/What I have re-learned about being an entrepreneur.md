---
created: 2021-12-09T18:13:20-06:00
lastmodified: 2021-12-09T18:19:26-06:00
---

# What I have re-learned about being an entrepreneur

Who is the customer? 

How are they a customer? Contributing employees? Actual paying users? Paying/contributing partners/affiliates? Bidders/shoppers/curiousFAQers? Investors? Landlords?

A banker is NOT the customer ... unless you are operating something like politburo-controlled USDA-FSA communist farming operation.
