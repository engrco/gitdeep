---
created: 2021-12-31T18:19:32-06:00
lastmodified: 2021-12-31T18:24:14-06:00
---

# Will

God's will be done.

You cannot really impose your will upon anything...EXCEPT for your attitude toward life ... you can will that you will always try to make the best of your situation and that you will never carp about things beyond your control, but instead will pray and ask for discernment.
