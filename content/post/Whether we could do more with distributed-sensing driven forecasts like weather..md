---
created: 2021-11-11T06:29:27-06:00
modified: 2021-11-11T06:31:24-06:00
---

# Whether we could do more with distributed-sensing driven forecasts like weather.

Got a sail for ur kayak?

If you enjoy a good breeze, you won't even need a lake, river or large puddle underneath you to just float downstream.

https://www.weather.gov/wwamap/wwatxtget.php?cwa=fsd&wwa=all
