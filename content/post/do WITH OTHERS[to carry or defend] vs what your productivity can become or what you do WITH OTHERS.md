---
created: 2022-02-10T12:39:26-06:00
lastmodified: 2022-02-10T12:45:57-06:00
---

# What you have [to carry or defend] ... vs ... what your productivity can become or what you do WITH OTHERS

When you get hung up on wealth inequality you turn yourself into a crybaby loser victim ... you don't have to be a victim unless you decide to work at it.

Life is neither endless nor fair ... and, therein lies the immaculate beauty of it.

https://en.wikipedia.org/wiki/Lorenz_curve
