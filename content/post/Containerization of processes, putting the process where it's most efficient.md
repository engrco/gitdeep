---
created: 2021-12-18T18:31:04-06:00
lastmodified: 2021-12-18T18:32:29-06:00
---

# Containerization of processes, putting the process where it's most efficient

TRIZ 40 principles of design thinking
