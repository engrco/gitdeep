---
title: Lord's Day #1
subtitle: ALWAYS ALWAYS ALWAYS Be Grateful
date: 2021-12-28
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# What is your only comfort?

ALWAYS give thanks for the blessing of the day; you are not entitled to even a dreadful, completely miserable day.

ALWAYS pray to discern the Creator's will; ALWAYS pray to be guided/assisted in doing the Creator's will.

ALWAYS forgive; ALWAYS allow the Creator to judge and exact vengeance. NEVER EVER allow yourself to carry hatred.
