---
created: 2022-02-15T19:40:29-06:00
lastmodified: 2022-02-15T19:44:37-06:00
---

# Astrobiology ... insights into formation of precursors of Life that may have happened billions of years ago

http://prebioticchem.info/

Production of Tartrates by Cyanide-Mediated Dimerization of Glyoxylate: A Potential Abiotic Pathway to the Citric Acid Cycle

https://pubs.acs.org/doi/10.1021/ja405103r?s=09
