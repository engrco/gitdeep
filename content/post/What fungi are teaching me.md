---
created: 2021-12-08T21:01:30-06:00
lastmodified: 2021-12-11T05:41:02-06:00
---

# What fungi are teaching me

... to sip from the firehose ... or the tsunami ... there are vast oceans of material and knowledge on fungi ... each single unique scientific paper is one drop in the ocean...that obviously doesn't account for the material that is a rehash of what is/was already known.
