---
created: 2021-12-27T21:29:26-06:00
lastmodified: 2021-12-27T21:34:05-06:00
---

# Good problem to have

If you're an individual, take Social Security EARLY ... you are not that likely to live past 85 or so, ie the point where the extra money recieved from delayed later distribution exceeds the money received by taking an early distribution ... but if you do indeed live past 85, longer life is a good problem to have.
