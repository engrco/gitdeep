---
created: 2022-01-06T06:37:37-06:00
lastmodified: 2022-01-06T06:39:12-06:00
---

# If you are actually good at what you do, you must work at put yourself out of business ... so that you are free to explore other things

The standard American diet is like a RELENTLESSLY beguiling parasite ... not merely tasty, but engineered to be addictive, in order to drive predictable product demand ... Big Food could almost be thought of as an addictive, evolving meta-virus of affluence.  

Ice cream and many processed and ultra-processed other foods that  humans either could not afford in the recent past OR foods that were just not as appealing, satiating or sustaining when people engaged in heavy physical labor for 12-15 hrs per day have been carefully engineered by food scientists to "taste so good" ... particularly to inactive, sedentary, stressed humans.

The large volumes of extra food consumption do two things ... predictable high volumes of demand drive predictable returns on investment [at least in the intermediate sense] which both spurs investment and inevitably [in a competitive industry] drives costs way down [which further increases volume demanded and hardens the addiction].

Of course, the consumption drives inexorable increases in insulin resistance, lipolysis, and hepatic glucose production ... which in turn drive susceptibilities to disease vectors such as mosquito/tick predation as well as also various other infectious agents, cancers and different accelerations of the atrophications of aging and senility ... which in turn guarantee increased DEPENDENCE upon Big Pharma, Big Medicine, health insurance and ultimately Big Government.
