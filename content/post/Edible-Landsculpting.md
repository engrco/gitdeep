---
title: Landsculpting For Carbon Sequestration
subtitle: 150,000,000,000 T per 150,000,000,000 A
date: 2021-12-28
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# Eliminate non-essential vehicular traffic, ie like 2020, but with INTENTION

Shift culture ... in a HARD, DELIBERATE shift ... AWAY from being backward, inbred, lazy and automotive-based TO being productive, managed, tracked and optimized in information-based workflows and lifestyles.

Work creatively in order to relax; work creatively on personal lifestyle in order to LIVE ... rather than just hopping in the car to drive around like a mindless idiot. 

Shift culture into being INTENTIONALLY, deliberately, quantitatively creative ... manage the information flow, measure and heat map it [like a drill-downable commitgraph], then aggressively optimize it

# Edible / pharmacological landsculpting rather than turfgrass

ELIMINATE most of the dependency upon transportation and logistics by landsculpting an edible foodscape.

It's pretty easy to sequester well over one ton per acre of carbon if we sculpt our lives with LIFE ... rather than mechanisms and industrially automated failure modes ... the key to sequestering carbon is by adding tons of LIFE to every human's share of landsculpted and LIVING environments.  

Start seeing failure-prone mechanisms as a human DISEASE that we can beat ... if we choose LIFE.