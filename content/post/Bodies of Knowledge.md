---
created: 2021-12-16T15:31:21-06:00
lastmodified: 2021-12-19T10:54:01-06:00
---

# Building Bodies of Knowledge

Read, read more, debate/argue, read more ... learn how to use machines to do the tedious explorative part of reading ... 

... learn how to machine think to optimize your reading...use / adapt algorithms as well as how to use / adapt oceans of curated datasets for machine learning.

https://the-algorithms.com/algorithm/logistic-regression
