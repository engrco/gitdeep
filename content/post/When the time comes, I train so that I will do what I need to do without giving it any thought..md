---
created: 2022-01-31T18:22:27-06:00
lastmodified: 2022-01-31T18:28:54-06:00
---

# When the time comes, I train so that I will do what I need to do without giving it any thought.

But my training is bound to be entirely deficient in some respects or another...so I pray that God will provide me with my "daily bread" ... when the daily bread I need is the courage, will and capacity to kill and/or die as the situation requires ... I long for the day when I can serve my Creator and return to be with my Creator in every sense ... I know that my Creator is with me always but I sometimes forget this or get distracted by the stupid examples of my deeply flawed pathogenic species.
