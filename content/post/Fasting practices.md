---
created: 2021-12-03T18:59:40-06:00
lastmodified: 2021-12-03T19:03:21-06:00
---

# Fasting practices

Journal to record thoughts

OMAD

Consume animal fats for satiety 

Maintain hydration, but expect ketogenic biochemistry to drive diuresis ... thus it is necessary to consume salt and magnesium supplement
