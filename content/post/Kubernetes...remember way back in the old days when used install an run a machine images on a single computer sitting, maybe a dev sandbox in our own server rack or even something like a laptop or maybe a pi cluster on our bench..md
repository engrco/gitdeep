---
created: 2021-11-19T21:03:13-06:00
lastmodified: 2021-11-19T21:19:56-06:00
---

# Kubernetes...remember way back in the old days when used install an run a machine images on a single computer sitting, maybe a dev sandbox in our own server rack or even something like a laptop or maybe a pi cluster on our bench.

The open sourcing of K8s [at OSCON in July 2015] really changed the game ....it's not just Docker or pods of containers or PaaS-agnostic microservices architecture ... which you could do back then ...  it's really the collaborative CI/CD dev ops workflows that things like Gitlab have made not just possible but the definion of best practices for collaboratuve asynchronous workflows that allow for around the clock development, operating and continuous testing in production.

It's hard to imagine what is was like when people just wrote / ran software that ran on their own machines ... but there are still people in slightly backwards developing economies that still do it the old way ... not in the Third World, but in places where people are incapable of learning better ways.

https://about.gitlab.com/blog/2021/11/16/gko-on-ocp/
