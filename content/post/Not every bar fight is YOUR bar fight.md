---
created: 2022-02-24T21:38:27-06:00
lastmodified: 2022-02-24T21:39:22-06:00
---

# Not every bar fight is YOUR bar fight

Some are.  Choose wisely.
