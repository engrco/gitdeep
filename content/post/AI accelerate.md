---
created: 2022-02-20T16:43:37-06:00
lastmodified: 2022-02-20T19:39:49-06:00
---

# AI ... Accelerating HYBRID Intelligence

# AI Accelerators

[Part I: Intro](https://link.medium.com/PK5RTRurOnb)

[Part II: Transistors and Pizza (or: Why Do We Need Accelerators)?](https://link.medium.com/E8I8PuqzOnb)

[Part III: Architectural Foundations](https://link.medium.com/2KxiE3uzOnb)

[Part IV:The Very Rich Landscape](https://link.medium.com/M6zYvvxzOnb)

[Part V: Final Thoughts](https://link.medium.com/rLA9ntzzOnb)
