---
created: 2021-11-25T21:27:59-06:00
lastmodified: 2021-11-25T21:39:10-06:00
---

# Old fucks live in the past

The natural tendency of any old person is to dwell in the past, to relive the comfort of rose-colored memories ... but when you give in and allow yourself to do that, you will suffer, suffer rather enormously and if you cannot knock it off,, you will rapidly lose control of your rights to look after your own welfare.

If you want a better life, you have to live in and [more importantly] sieze responsibility for driving and IMPROVING your future...rather than trying to justify, recapture, rework or relive your past ... the future is your best road ahead on your best horizon ... you get to choose the road and how you going to drive, but you cannot live or drive looking back -- except for zombies nobody is are going that way.
