---
created: 2022-02-16T22:09:55-06:00
lastmodified: 2022-02-16T22:21:31-06:00
---

# Respect your ancestors as HUMANS

Honor your mother and father ... NEVER worship them ... do not put them on an idolatrous pedestal ... reserve that DEEP / unfathomable / abiding LOVE for your Creator ... LOVE and RESPECT your parents for doing the best that they were capable of.

The ancient wisdom of loving and cherish your God more than you love life itself is about KNOWING YOUR UNIQUE WHY ... when you think about how it affects you and your soul, you will realize why the process of prayerful discernment is the most holy prayer that there can possibly be.
