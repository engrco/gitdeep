---
title: ENGRco
subtitle: On-Demand Engineering Collaborators
date: 2021-01-01
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---


ENGRco is like a temporary service firm ... except that actually providing the job-location service is kind of the responsibilty of each collaborator ... although the GYGbot project is aimed to make the matter of matching engineering talent with needs for that engineering talent somewhat easier.  

ENGRco engages in open source engineering and engineering data analysis ... for example, one project involves analzying the vast amount of data streaming from the JWST and making some sense of the observations, ie not just to extend the [Draper catalog](https://thereaderwiki.com/en/Henry_Draper_Extension) of primarily earth-based observations of stars by at least six or seven orders of magnitude or to map, characterize, analyze the systems of planets orbiting the next three thousand quintillion stars but to also explore fundamental Physics of energy, matter, space and time.  Another project involves extending and elaborating upon the reliability engineering Body of Knowledge. 
# [Engineers must engineer their own engineering skillsets](https://twitter.com/QualityFellow/status/1475842689425981450)

FOCUS upon improving your net value to others, especially focus upon those you know best, ie not necessarily your family or neighbors, but those who KNOW ... focus most intently on yourself, your finite life, your finite resources

Become ruthlessly efficient ... even risk being pennywise, but think in terms of extending your costcutting to other pursuits ... make a sport of lowering your bills in order to help others lower their bills ... be FRUGAL ... embody the best in an extremely frugal, but creative lifestyle ... creativity is about ideas and how you think -- it never requires significant amounts of money, but you must stay alive ... so work intently on demonstrating significantly more creative, lower cost lifestyles ... consuming so much makes you fat and uncreative, you can't swallow your way to intelligence.

LISTEN globally, reach out to others with shared interests ... completely hard ignore those who are gossips, tattlers, or conformists ... seek to understand, to gather intelligence, to discern how you can be of greater value.

Ruthlessly minimize vulnerabilities, relationships and things that drive costs, ditch baggage and make others carry the obstacles of material wealth, power, fame. You must do this in order to maximize net value and to be more fit and able in the future to FOCUS even more intently on improvement in net value.

## The ONLY asset that anyone owns is their time

Use a sliding scale of time commitment to maximize the value of today and your future days ... NEVER EVER EVER look back ... especially in regret or longing for the past.

A) Use a "Sharpen the Saw Sabbath ... 10 hrs of orchestration, prioritization and deeper reflection each week ... it may make sense to fast on this day.

B) Know your top 5 priority projects in order to allocate your best 20 hrs per week ... 6 of your vest best hrs of your very best time to the top priority ... then five of your remaining best hrs to the next, then 4, 3, 2 best hrs the the remaining three of your top priorities. Obviously, you must KNOW your priorities and your best work hours to do this.

C) Your next 20 priority projects will be allocated 20 hrs of time per week, although not necessarily and equivalent 1 hr/wk ...the highest amongst these middle priorities will get more time ... for example, the top 10 of these 20 might get 90 minutes with slightly more going to the highest, whereas bottom 10 get only a minimal check/review/reprioritization ... sometimes the best thing that you can do for a project is to leave it alone -- for example, in writing activities it is necessary to leave things "go cold" to come back to review with fresh, ruthless and ice-cold perspective.

D) 35 hrs of prayer / walking / reflection / reading each week ... this amounnts 5 hrs/day EVERY DAY, including "Sharpen the Saw Sabbath"

E) Allocate 10 hrs of housework/5S/maintenance chores each week ... on average, this might be about 100 minutes per day during the week, but these chores will have to match the opportunities that are presented, ie there might be time that works better for mowing lawn, whereas cleaning the bathroom might be something one does when one can't sleep

F) Allocate 10 hrs of time for "power naps" during week ... these breaks should never be scheduled, but instead, brief rest is allocated on as-needed basis and it's not something to apologize for.

G) Devote 9 hrs of time each day to sleep / rest hygiene / and wake-up activities, eg just watching the sunrise, each day ... 63 hrs/wk