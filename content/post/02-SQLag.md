---
title: SQL.ag
subtitle: Soil Quality Laboratory
date: 2021-01-01
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# [Invest in improving your planet](https://twitter.com/QualityFellow/status/1475842689425981450)

FOCUS upon improving the quality of LIFE for all beings on the planet ... most of the stuff that humans do might actually destroy life or mindlessly ruin it, because humans are not paying attention to something magical that our Creator has provided.

Soil quality is primarily about paying closer attention to LIFE ... especially to the life immediately around you ... thus soil quality laboratory is about ceasing to enable the mindless fucks who are just refuse to pay attention.