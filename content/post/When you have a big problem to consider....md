---
created: 2022-02-26T06:02:03-06:00
lastmodified: 2022-02-26T06:18:03-06:00
---

# When you have a big problem to consider ... pray for discernment and go for a walk

Never be OLLOW anyone ... especially not the news.
But NEVER not FOLLOW anyone ... especially not the news.

Unless it's a tiny problem.

You might have to kill someone on your walk ... but unless they attack you, first you don't need to 

Bur you won't have walk for centuries ... because you won't live for centuries...even if you walk a lot.

But ... when you are stupid enough to FOLLOW the news coverage ... you will DESERVE to get really depressed and die sooner, but you can't count on just dying that easily

When you FOLLOW the news, you will be tortured a lot needlessly and you might last a long time .. but that kind of needless suffering your own fault for not walking but instead turning on the news.

There are other distractions that aren't so bad ... you can work on things ... OR think about stuff that you CAN work on or extend  ... like [three mirror anastigmat](https://en.wikipedia.org/wiki/Three-mirror_anastigmat) optics, hyperspectral imaging or laser inferometery

You should FOLLOW anyone ... especially not the news.
But NEVER not FOLLOW anyone ... especially not the news.
