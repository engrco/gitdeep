---
created: 2022-01-30T07:13:50-06:00
lastmodified: 2022-01-30T07:22:49-06:00
---

# Competition, competition, competition

It is really a matter of sharpness...in order to sharpen the knife, it is necessary to hone skills through rigorous test ... through production, through competition, through battle.

Although I apologize for the use of the militaristic language ... there simply isn't a better way to convey the intensity of the abrasive grind necessary to get sharper ... and beyond getting roughly sharper, it is necessary to hone, sand and polish ... to continually pack your gear and re-pack your gear tighter.
