---
created: 2021-12-06T16:40:39-06:00
lastmodified: 2021-12-06T16:52:24-06:00
---

# DAOts

I generally find most tech things intriguing or mildly interesting ... I completely love turning over the ideas of anything that involves transactions and contracts in my head ... I could spend ALL day and then some in the theoretical virtual realm of ideas about markets, commerce and accelerated negotiations.

But ... PRACTICALLY ... I do NOT trust the stuff ... I also completely distrust material stuff, property and BURDENS masquerading as assets ... but I trust distributed bullshit even less ... because parasites like Farnam Street blog and other subscriptions based services make it effectively IMPOSSIBLE to terminate a fixed-length contract and this sort of problem with minor practical details gets much worse in the virtual realm.

https://networked.substack.com/p/web3-i-have-my-daots
