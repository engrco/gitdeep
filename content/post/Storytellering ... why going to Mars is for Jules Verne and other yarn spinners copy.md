---
title: Integrate notetaker Google Keep with Git and NewsBlur
subtitle: Using Multiple Devices to Commit
date: 2021-11-06
tags: ["example", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# Storytellering ... why going to Mars is for Jules Verne and other yarn spinners

It is worth emphasizing that stories MATTER ... a lot ... maybe more than grandiose projects that are bound to prove that dropping into a new gravity well is just dropping down a WORSE gravity well than the one we live in on Earth.

However... ideas do MATTER ... it is just that there are plenty of opportunities to test ideas with STORIES which generally tend to sorta abide with the generallly accepted principles of Economics or Physics ... that is the first test of a good theory ... those stories should captivate and encourage different ideas ...  the models which push across boundaries tell us [as in the Kuhn-Tucker conditions] the cost of a constraint ... facts are stubborn things, but we can find out the cost of something ASSUMED to be a fact but is nothing more than a **generally**-accepted assumption.
