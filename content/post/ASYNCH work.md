---
created: 2021-12-14T15:23:39-06:00
lastmodified: 2021-12-14T16:22:07-06:00
---

# ASYNCH work

“People who enjoy meetings should not be in charge of anything.”

Thomas Sowell


I do not go out of my way to avoid physical human interactions... but I do do tend to avoid EVER going out of my way just for the sake of participating in hanging out with people.

People can't really help it but humans are just gawddamned fucking dumbasses in person ... the interactions are far worse than interactions that are just about the MEAT of the topic, like a debate over some idea or paper or data ... I know that I am the worst, dumbest version of myself in person and my guess is that my inadequacies is probably ordinarily the case ... I don't understand why people want to go the bother of TRYING to meet in person ... but we should just make the best of it when we are sorta forced into the proximity of having others in our presence.

It's not just the meeting of people and the invasion of personal space, but what's the point of gathering in order to interact with humans for exactly WHAT? Hopefully, it's not some meeting or some other manipulative peer-pressure bs to enable the predators to take advantage of prey animal gregariousness, ie something like a church service? 

The real problem is in just getting there 
... a NON-VALUE-ADDED waste of time. One has to drive/fly/travel, which is actually much more dangerous than swapping saliva with random people and generally a chore... so much worse and dumber than even hanging out with human beings.
