---
created: 2022-01-18T20:06:01-06:00
lastmodified: 2022-01-18T20:06:33-06:00
---

# Mood / mysterioso

I am wondering what the secret ingredient of Lalo Schifrin's music is/was ... the 5/4 time signature ... or just the ridiculously good studio musicians and care in eliminating the noise.

It's almost hilarious to listen to the old Schifrin music and feel that mysteriosa suspense vibe ... even if it's like a cartoon.
