---
created: 2022-01-30T14:44:46-06:00
lastmodified: 2022-01-30T17:22:37-06:00
---

# What are you watching?

And why do you tolerate those advertisements in the background ... or worse, ie pretend that they aren't effective at programming you to feel inadequate and maybe not generating enough cash to have things you want.

In other words, why do want anything...you either NEED it or you don't.
