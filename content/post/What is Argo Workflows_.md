---
created: 2021-12-01T15:09:58-06:00
lastmodified: 2021-12-01T19:05:35-06:00
---

# What is Argo Workflows?

https://github.com/argoproj/argo-workflows

Argo Workflows is an open source container-native workflow engine for orchestrating parallel jobs on Kubernetes. 

Argo Workflows is implemented as a Kubernetes CRD (Custom Resource Definition).

Define workflows where each step in the workflow is a container.

Model multi-step workflows as a sequence of tasks or capture the dependencies between tasks using a directed acyclic graph (DAG).

Easily run compute intensive jobs for machine learning or data processing in a fraction of the time using Argo Workflows on Kubernetes.

Argo is a Cloud Native Computing Foundation (CNCF) hosted project.
