---
created: 2022-01-06T06:44:08-06:00
lastmodified: 2022-01-06T06:53:57-06:00
---

# The stupid that I can't fix is the stupid of relentlessly returning to what is comfortable

I can fix most forms of stupid by simply doing something, offering that example and trusting that people will eventually become smart enough to imitate what works.

The fact that I am alive at all ... it is ONLY because I am not stupid ... and that I have eschewed those behaviors which drove me to that point where I was told that hours or days to live, but not weeks.

When I was informed of my imminent demise, my only thought was that it was not imminent enough. I didn't want to havr to live hours or even minutes -- I was so miserable that hoped for a death was seconds away, if not right NOW.
