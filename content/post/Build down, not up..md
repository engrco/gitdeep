---
created: 2022-01-26T11:52:42-06:00
lastmodified: 2022-01-26T11:59:59-06:00
---

# Build down, not up.

In a nutshell ... we humans fucked up MASSIVELY when we gave up caves or ceased to make caves more habitable and comfortable ... these are not simply low-tech hole or burrows ... but instead they should be seen as highly engineered, reinforced, antifragile, impenetrable structure ... insurgents that dig and construct re-inforced underground/cave structures continue to defeat enemies who are stuck fighting on or flying above the surface ... Vietnam [French or USA] and Afghanistan [USSR or USA] are recent examples.

Why truck in construction materials or cater to those who want to build a monument or structure to flaunt wealth? What's necessary for life/comfort?

Building up, rather than down unnecessarily burns up tons of resources for each resident and is far more fragile, ie subject to either the elements or fire.

 If we can figure out how to move air, we sure as fuck can figure out to pump water / or exhaust fumes to the  surface ... why not USE the whole surface of any property as a park rather than waste the sunlight on a structure that is going to be sink of incoming solar radiation?
