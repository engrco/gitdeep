---
created: 2022-02-12T15:45:39-06:00
lastmodified: 2022-02-12T15:54:06-06:00
---

# Think in semiotics ... pictures, mathematics, music, concepts ... rather than words

Train in order to develop stronger discipline; discipline equals freedom.

https://business-review.eu/education/how-to-manage-your-college-workload-222687he opportunities you have not yet recognized, b) to forgive and eliminate all hate, especially self-hate or self-doubt, c) to discern the best path free of temptation to fall back into weakness, addiction or distraction.

3) Manage and prioritize your day in order to not be a wage slave.

4) Train in order to develop stronger discipline; discipline equals freedom.

https://business-review.eu/education/how-to-manage-your-college-workload-222687
