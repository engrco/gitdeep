---
created: 2021-12-06T04:13:22-06:00
lastmodified: 2021-12-06T04:14:09-06:00
---

# Gregory Fridman's simple vid series

My post here is not about the specific paper that Gregory Fridman is reviewing OR any kind of policy prescription on this particular matter ... it's not about the keywords in the title ... it's about the elegant demonstration of the THE METHOD.

The METHOD Gregory uses is not one bit controversial or complicated. 

It is simple, but sufficiently rigorous to be useful, even powerful...people who have mastered it might not be ready to pursue research grants or develop workable business plans for a scientific venture, but they will be able to get along, on their own, without having other people think for them.

So I am unambiguously and strongly, strongly, STRONGLY in favor of the methodology ... particularly for pedagogical purposes and teaching people [anyone over the fourth- or fifth- or sixth-grade reading level] ... it what ordinary, functioning adults should  be able to do ... ORDINARY people who want to be independent in this culture need to familiar with the basics of the scientific method and critical review.

The point is NOT to just watch Gregory's video series, although you should consider doing that [especially, focusing first on the papers/vids that match your particular range of interest] ... browse the series as you normally might and delve into a couple to examine the METHOD.

... the POINT is to learn the self-directed method YOURSELF ... and then be able to USE that method of examination and social presentation, taking advantage of social media tools to encourage debate/commentary and help improve your understanding of topics [as well as the understanding of levels of awareness on topics].

That should not be at all controversial ... this methodology or something like it should look very familiar to you ... you have the pieces; you should be able to put it together ... but here's the controversial part ... if you are unable to begin to do this kind of independent, on-your-own, self-directed study and scientifically-based review and presentation, then you are either: 

A) a moron who should be living in a managed-care environment, OR
B) someone who has been programmed by your educational system to be functiionally equivalent to a moron, unable to think independently, devise a simple review of scientific literature on your own and be able to thinker your way to the the bottom of anything of serious importance to you.

You don't need to be a physicist like Gregory ... but you really HAVE TO be able to do this kind of thing, ON YOUR OWN ... in order to be able to THINK your way through modern life, without being held captive of those who would have you be too DEPENDENT, unable to reason or think scientifically in a world/culture that is overwhelmingly driven matters that require rigorous scientific thinking and scrutinization on the part of masses of capable individuals.

There are reasons to NOT be a specialist ... Robert Heinlein has a famous list of basic things that you should be able to do ... but Heinlein's list was for a CENTURY AGO, probably intended for someone in the 1952 or so.

You might want to retreat to the comfort of Heinlein's old list ... but you must prepare for live in the middle of the 21st century, or else. 

Don't look back, we are not going that way ... it's already basically 2022 ... not 2012 ... not 2002 ... NOT 1992 ... DEFINITELY NOT 1982 ... and very little at all like 1972 any more.  If you insist on behaving like it's still 1962, the odds are good that you may belong in a nursing home, under supervision, but able to watch your favorite programs on the teevee.

Develop the skillset for 2032, 2042, 2052 ... because it will be 2052 before the clock EVER goes back to 1992.
