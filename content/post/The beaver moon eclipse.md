---
created: 2021-11-18T10:11:02-06:00
lastmodified: 2021-11-18T10:11:27-06:00
---

# The beaver moon eclipse

The weather, radar and astromy apps ARE getting better though ... not really quite as useful as living outdoors and more thoroughly immersed in [unable to get away from] life/survival IN the Universe ... for example, tonight at 3 AM is LONGEST partial lunar eclipse in 600 years ... interesting perhaps, but hardly the same as a native sense of direction [under the night sky] and awareness of exactly where one is as a sea-faring nomad ... but consider the life-skills of being able to block spam from robocallers ...

https://www.space.com/beaver-moon-lunar-eclipse-2021-guide
