---
created: 2022-02-17T11:51:27-06:00
lastmodified: 2022-02-17T12:12:40-06:00
---

# Git-ify Deep Learning

The deepest of deep learning is about using the intelligence of human communities.

This is HYBRID intelligence...developers bebating ideas, working both on their own AND together with human intelligent work augmented by machines doing lower-value tedious tasks ... dev communities add the most value to humans and the community when developers use technology to make human learning more efficient.

In our business, human work is compensated per the proof-of-work as demonstrated by that human's commitgraph ... each human involved owns the entire project, but each/every license to use and extend the code base is constained by the project's open source license.

Of course, the codebase can be forked but the most intelligent development will necessarily be community-based and richer/deeper because of the deeper emotional/technical intelligence of working more effectively together.se is constained by the project's open source license.

Of course, the codebase can be forked but the most intelligent development will necessarily be community-based and richer/deeper because of the deeper emotional/technical intelligence of working more effectively together.
