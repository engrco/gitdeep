---
created: 2021-11-30T03:39:16-06:00
lastmodified: 2021-11-30T03:40:38-06:00
---

# NYC was built on TRADE, not land

Value is not in a fixed rock, it is the flow of value.
