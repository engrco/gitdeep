---
created: 2021-12-30T18:07:57-06:00
lastmodified: 2021-12-30T21:20:46-06:00
---

# Heroic efforts

Do heroic efforts win games ... or are they only heroic, like putting up a brave front, ie just an egocentric liability in a real battle or long war?

Wisdom, concepts, ideas >> knowledge, skills, arts >> facts, clever arguments, stories >> consumption, material wealth, possessions 

https://youtu.be/r0_f6xPI-a8
