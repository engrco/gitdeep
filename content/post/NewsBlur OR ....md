---
created: 2021-12-17T19:02:21-06:00
lastmodified: 2021-12-19T10:48:59-06:00
---

# NewsBlur OR MetaNewsBlur

Sifting ... sorting ... learning about machine learning ... before we just delve in and learn something about what has been done, what can be a good exercise and giving these learning materials a solid go ... 

... let's START with exploring the overall lay of the land in terms of what is out there already and what will soon be out there ... after all, if we really want to learn [about machine learning] we should think about how people are learning and how we can help others learn.

https://en.wikipedia.org/wiki/List_of_datasets_for_machine-learning_research
