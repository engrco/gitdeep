---
created: 2022-01-30T07:24:44-06:00
lastmodified: 2022-01-30T07:31:24-06:00
---

# A manual for being natively nomadic

AncientGuy.Fitness is future fitness.

It's fundamental about health, fitness and skills to be natively mobile ... to be independent, always dangerous, predatory when the situation demands or certainly capable of bringing the fight to an enemy ... but always at ease, comfortable in Nature, comfortable in death and, in an emotional sense, at rest.
