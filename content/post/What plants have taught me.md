---
created: 2021-12-08T07:29:06-06:00
lastmodified: 2021-12-08T07:54:11-06:00
---

# What plants have taught me

Plants respond to widely varying weather and other conditions, eg prairiefires, to make the best of the entire set of conditions they are given ... plants adapt and thrive -- what humans see as an individual plant might die, but plants are not really about the individual -- plants are like genetic appendages of different limbs of the same body descended from our common Mother.  Humans see separation in individuals ... plants live, compete, experiment by either thriving or dying in order to further the cause of our common Mother ... plants photosynthize convert largely unusable radiation, using water and carbon dioxide, into stored energy reserves ... the reserves are ultimately harvested by bacteria and fungi, sometimes inside of animal digestive systems, and converted into more complex proteins and building blocks for less sessile life forms [which could not exist without plants].

Humans study plants from the selfish perspective of what can that plant do for me ... but we do not tend to "consider the lilies" from the perspective of what can that plant teach me about being less needy, less entitled, less demanding of making things much less elegant but something we can point to say, "I built that."

No human has ever built anything remotely even a fraction as elegant as a tree ... we can only fuck with shit ... and yet, we tend to appreciate only the shit stains that assholes have left on the Universe as high art or science.
