---
created: 2022-02-02T19:43:55-06:00
lastmodified: 2022-02-02T20:13:01-06:00
---

# Self-reported physical inactivity

https://www.cdc.gov/physicalactivity/data/inactivity-prevalence-maps/index.html
