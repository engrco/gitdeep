---
created: 2021-11-15T08:58:28-06:00
lastmodified: 2021-11-15T09:31:34-06:00
---

# Newsreaders, Twitter, Commit Messages

Reading than podcastering ... although it's important to distil and produce listenable content ... it's the distillation that matters more ... the audio quality is not really something that sticks over the ages ... pithy quotes matter beyond what seems reasonable ... video is much more ephemeral than people might want to admit ... in my personal experience, the only thing I seem remember about television watching is the waste of it ... there just aren't that many teevee moments that like the first landing on the moon or the shooting of Martin Luther King Jr, ie the best of the best memories are more along the lines of the silliness of Gilligan's Island ... the more recent popular shows like Mad Men are memorable only for the depressing depravity of the characters, ie my parents, uncles/aunts, older neighbors were far better role models to watch/imitate.
