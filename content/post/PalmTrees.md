---
title: Palm Trees
subtitle: Be able to acclimatize or stay the fuck out of my life
date: 2022-01-08
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

I find difficult, contentious, extreme situations to be EXTREMELY USEFUL for helping me sort out what I should keep, what I should never ever trust again.

If you are a palm tree ... you will die and I am not going to cry at all ... I don't try to maintain hot houses for palm trees.
