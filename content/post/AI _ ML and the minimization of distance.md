---
created: 2021-11-28T21:47:56-06:00
lastmodified: 2021-11-28T21:52:14-06:00
---

# AI / ML and the minimization of distance

Almost all intelligence gathering and knowledge engineering done by machines is going to at some point come down to patterns, measurement of fit and the minimization of distance between a theoretical pattern and the mean/median/mode pattern.
