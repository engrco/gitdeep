---
created: 2021-12-06T20:04:48-06:00
lastmodified: 2021-12-06T20:15:49-06:00
---

# If you want to stop helping rich, greedy, covetous people

You HAVE TO stop valuing what they have AND you have to stop wasting any time desiring wealth or anything that you imagine gives you power over wealthy people.

Humility and anonymity are the only defense against being held hostage by any desire for wealth or comfort.
