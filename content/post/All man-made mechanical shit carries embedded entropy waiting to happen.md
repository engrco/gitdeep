---
title: All man-made mechanical devices embed entropy
subtitle: Mechanisms are failure modes waiting to happen
date: 2021-12-28
tags: ["example", "bigimg", "TBD0"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

You can WORK LIKE A FIEND maintaining, fixing,  reworking to put off the inevitable entropy of mechanical systems ... but unlike a biological ecosystems with phosynthesizing plants, mechanical shit is never going heal, feed or fix itself ... it only going to make a lot of noise to remind you it's out there, wearing itself out and slowly destroying itself.  

Inevitably, all mechanical systems will achieve their own destruction ... perhaps taking along the species that created and worships those systems.
