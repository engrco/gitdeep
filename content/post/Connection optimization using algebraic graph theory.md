---
created: 2022-01-21T15:43:26-06:00
lastmodified: 2022-01-21T20:27:56-06:00
---

# Connection optimization [or accelerated negotiation of convergence toward optimum] using algebraic graph theory

https://en.wikipedia.org/wiki/Algebraic_graph_theory

https://arxiv.org/abs/2112.05935

Control-Tutored Q-learning (CTQL)

https://arxiv.org/abs/2112.06018?context=math.OC

Quantum Interior Point Methods for Semidefinite Optimization

https://arxiv.org/abs/2112.06025?context=math.OC

Extending AdamW by Leveraging Its Second Moment and Magnitude

https://arxiv.org/abs/2112.06125?context=math.OC

Trust-region algorithms: probabilistic complexity and intrinsic noise with applications to subsampling techniques

https://arxiv.org/abs/2112.06176?context=math.OC

On the Heterogeneity of Independent Learning Dynamics in Zero-sum Stochastic Games

https://arxiv.org/abs/2112.06181?context=math.OC

Restless Bandit Model for Energy-Efficient Job Assignments in Server Farms

https://arxiv.org/abs/2112.06275?context=math.OC

Gamifying optimization: a Wasserstein distance-based analysis of human search

https://arxiv.org/abs/2112.06292?context=math.OC
