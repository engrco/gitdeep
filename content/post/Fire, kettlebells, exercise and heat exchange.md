---
created: 2022-01-20T10:27:24-06:00
lastmodified: 2022-01-20T10:34:54-06:00
---

# Fire, kettlebells, exercise and heat exchange

What if cast iron sculptures glowed to keep pipes from freezing?

What if we used an attached porch or possibly the garage ... someplace where it's possible to tolerate higher CO levels because the space is drafty and nobody is going to spend that much time there... so we could use that space to provide a smallish, open fire tp heat up dumbells, kettlebells or Dutch ovens full of water... for the thermal mass that would keep the pipes from freezing.
