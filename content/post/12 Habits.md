---
created: 2022-01-26T12:03:34-06:00
lastmodified: 2022-01-26T12:14:46-06:00
---

# 12 Habits

1. Pray / Meditate / Walk More
2. Eat smarter/less and fast
3. Breathwork / cardio / oxygenation
4. Iterate more on 25 projects / 10 commits per day
5. Get smarter about networking/outreach
6. Train / roll / punch / kick / train
7. Get smarter about intell gathering/ GitRxiv
8. Explore / "sniff out" space / create / our Father who art in heaven
9. Explore inventions / technologies / ideas / creations
10. Music / data visualization / Figma SVGs or more logical visual menu-building
11. Spend more time in Nature / landsculpt / build soil
12. Be funnier but aggressive about challenging people to think / develop GENUINE compassion or empathy by getting people to stop whining and make the best of their futures
