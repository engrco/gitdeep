---
created: 2022-01-28T14:03:52-06:00
lastmodified: 2022-01-28T14:04:54-06:00
---

# Does crowd sentiment matter?

"Hey, hey, hey ... HEY!" 

Big dogs do not evolve from small ones ... not even crowds of small ones.


https://www.nature.com/articles/d41586-022-00209-0
