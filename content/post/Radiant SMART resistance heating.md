---
created: 2022-02-22T03:54:53-06:00
lastmodified: 2022-02-22T03:58:46-06:00
---

# Radiant SMART resistance heating

Not convection or conduction ...

... but surroundings that glow IR ... along with resistance heating of water pipes and things that require temperature in excess of 35F

MOSTLY...humans should generate their own heat ... to be healthier or to have a body physiology that continually adapts to ambient surroundings
