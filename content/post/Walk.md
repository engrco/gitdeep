---
created: 2022-01-28T20:00:25-06:00
---

# Walk

Walk, walk, walk ...walk more but hike/climb with purpose or weight ... walk fewer junk miles; instead lift more or roll more or work more on flexibility and balance in difficult walking.
