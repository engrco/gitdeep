---
created: 2022-01-17T17:40:58-06:00
lastmodified: 2022-01-17T17:48:38-06:00
---

# Terraforming For Dummies

First step: Focus on finding the set of several thousand big rocks in space that behaves or could behave somewhat like Earth.

Step two: Work out a way of getting to the ten or 100 that are closest ... ordinary, that's going to involve something like Noah's Ark ... except that your arks are going to involve several lifespans of travel of your arms to the candidate big rocks which appear to behave like Earth.

https://arxiv.org/abs/2111.05161
