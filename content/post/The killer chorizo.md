---
created: 2021-11-18T06:00:47-06:00
lastmodified: 2021-11-18T06:26:05-06:00
---

# The killer chorizo

Every single day ... I find that I have much less respect for anything built by humans ... and much more respect for the life that have been developed by our Creator.

Humans are worse than squirrels or pigs, nothing more than just oblivious swallowers or damned parasites with gigantic egos about what idiotic soy chorizos they have cooked up.

It takes a gargantuan portion time, probably decades, maybe centuries or millennia to get the coorect materials, substrates, life networks, supporting ecosystems to grow certain kinds of discriminating fungi ... morels are not a paint-by-number crop for toddlers like corn or soybeans.

Some mushrooms have a less discriminating palate, but as a rule they are particular about the fibers they colonize (ie it's not just the molecules but the structure/texture they use to build their interconnecting skeletal networks) ... it's not just that they won't be able to digest just anything, they also won't be able to have something like a backbone or physical structure to continue to gather nutrients and feed ... they appear sensible to something with a short lifespan like humans, but fungal networks are not paralyzed [over longer periods of time] ... so they are not like humans or hogs that will gleefully swallow any soy slop with a little flavoring ... their needs dictate their taste preferences.

More than just what is included; it might well be a matter of what is excluded or what is bound/unavailable because of some other bioflocculant or biocoagulent ... so growing healthty, sustainable fungal ecosystems is probably a long term slo detox thing, ie more than just getting the right nutrients and fibrous feedstock polymers into the chorizo -- also a matter removing the poisons like soy has in its estrogenic components.

Around here ... fire, likely resulting anthropogenic habit-destruction🔥 to make hunting easier has built the prairies over thousands of years ... as a result, we don't really have morels anywhere, except for isolated woodland pockets that escaped fire ... until we ditch the idiotic reverence for other hominid lifeforms, we won't be able to have nice things like forests and deep ecosystems because pyrolytic compounds or derivatives, or petroleum-like exudates are likely to be toxic or growth-stunting to things like morels or the species they feed on.
