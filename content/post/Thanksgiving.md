---
created: 2021-11-25T04:05:05-06:00
lastmodified: 2021-11-25T04:12:14-06:00
---

# Thanksgiving

The PROCESS and RITUAL of the improvement in one's daily habits to give thanks..

EVERY single day should BEGIN with things that one is thankful for and, especially,  for those things might want to work harder to improve.

1) Health/Fitness 

2) Skills/Capabilities 

3) Networks/Relationships 

4)  Nature/Universe

5) Opportunities To Serve
