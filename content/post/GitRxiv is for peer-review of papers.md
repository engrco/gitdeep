---
created: 2022-01-13T07:53:12-06:00
lastmodified: 2022-01-13T07:56:41-06:00
---

# GitRxiv is for peer-review of papers

Reading, curating and connecting the dots in serious papers is what GitRxiv is about ... but you can clone the repository and roll your own content curating and intelligence gathering tool

https://twitter.com/jbhuang0604/status/1481262433117343749?t=DG5ytaAhValGTOYclZmlog&s=19
