---
created: 2021-12-16T01:48:56-06:00
lastmodified: 2021-12-16T01:49:16-06:00
---

# RELIABILITY

So ... it appears that utilities that have invested burying power lines actually don't have any customers without power ... we should probably be cheering utilities that understand RELIABILITY.  But, it's funny how that works ... people must assume that utilities with buried power lines didn't get any wind.

https://poweroutage.us/area/state/iowa


Of course there are outages ... which oddly enough, show up in rather predictable jurisdictions.

Those jurisdictions are driven by the service areas of "your renewable energy future" utilities which invest in solar/PV along with lots of poles and aerial transmission lines to carry nonexistent wind/PV power back to the grid ... along with HEAVY investments in the best lobbyists in Iowa and campaign contributions to buy Governors, Legislators and County Supervisors ... RATHER than digging in powerlines.

Not burying powerlines provides different kind of reliability ... we see pols RELIABLY praising utility companies for bravely working to address the snapped off poles and aerial lines which were a known vulnerability, long before this storm was a gleam in any storm chasers eye.

This is because the rule for pols is NEVER LET A GOOD CRISIS GO TO WASTE.

It kinda rhymes with a catchy little phrase like "your renewable energy future" OR the policy of windmills on license plates, driver's licenses OR slapping Mid-American Energy on a football field that should be named for Jack Trice OR naming Alliant Energy centers for other popular sports teams.

So tonight ... the results show up ... Alliant has 78,000 people without power ... https://poweroutage.us/area/utility/829 and Mid-American has 36,000 out https://poweroutage.us/area/utility/714

That's the RELIABILITY of "never let a good crisis go to waste."  And, if you think this storm is some sort of unique aberration ... well, aren't you the reliable one?
