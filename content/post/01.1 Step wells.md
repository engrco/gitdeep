---
created: 2021-12-23T06:09:03-06:00
lastmodified: 2021-12-23T06:15:42-06:00
---

# Step wells

I think I might have a new sculpting project ... a terraced stepwell ... MAYBE ... I am not that sure yet ...

It'd mostly just be like a gym, for a daily workout ... a digging + farmers-carry workout ... something to keep me in digging/carrying/climbing shape, over the next few years/decades.

At first, it would just be borrow pit ... a few cubic feet of soil would be used, each day, on the rest of the yard ... but if I live long enough, it would step down deeper and deeper ... with anchored landscaping blocks securing the edge ... before it got very deep, I think that I would put something like a steel structure to make it possible to cover over it ... so from the surface it'd maybe something that looked like a patio surface ... underneath, it'd be like a deep basement, or cellar, or tornado shelter.

Ultimately, I am more intrigued by the opportunities for geothermal than just water ... of course, water for irrigation would be a benefit also ... but I mainly think about drilling wells [at the bottom of the cellar] and then using the groundwater for cooling/heating a heat pump in summer/winter.

https://www.bbc.com/future/article/20211012-the-ancient-stepwells-helping-to-curb-indias-water-crisis
