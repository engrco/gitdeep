---
created: 2022-02-10T18:09:07-06:00
lastmodified: 2022-02-11T18:17:21-06:00
---

# Why not use BOTH?  Use Gitlab as a primary, GitHub as a mirror [to be included in search results].

And that also allows you to use / extend Git itself if that proves necessary.

https://twitter.com/QualityFellow/status/1491929556948860963
