---
created: 2022-02-16T18:52:24-06:00
lastmodified: 2022-02-16T22:45:08-06:00
---

# Compare Israel to IowaMN

# Israel

9.48 million people; $410.5M GDP

# Iowa-Minnesota

8.90 million people; $566.1M GDP

# Then compare Israel to a nation like Rwanda or Canada ... Canada, like Israel, has always been a nation of REFUGE, for people fleeing from something, maybe boredom but often fleeing from legitimate, hardcore oppression.
