---
created: 2021-12-06T11:49:09-06:00
lastmodified: 2021-12-06T12:10:31-06:00
---

# What to do if you wake up with $1B

When you never have to worry about your needs again...you should realize that because you need NOTHING...you are finally, finally, finally free to disappear.

It will not be AT ALL easy. 

Become anonymous will take enormous discipline and constant effort to achieve this ... but when your NEEDS are satisfied, you can work at eliminating the things you might now see as needs.

Start your aggressive movement toward anonymity by aggressively removing all traces of anything that is remotely similar to conspicuous consumption ... then remove your reliance upon things like using a vehicle or needing utilities.

Then train to yourself 10X, 100X, 1000X harder, tougher, less dependent upon anything or anyone ... as rapidly as possible you must eliminate the distractions from centeredness, connection with your soul and your Creator ... DISAPPEAR, DISAPPEAR, DISAPPEAR.

You can still use things like phones, credit cards, social media ... the important thing is to be completely non-dependent upon those things ... use them ONLY to communicate,  in a very general, abstract, deeply spiritual sense -- but make it crystal clear that you are not there to provide material support or to bail anyone out of their self-inflicted jams any more ... you can still be available if they literally come and find you -- but it's not your job to fix the world's fuckups and needy souls who can't fend for themselves ... people must be able to go without you or needing to blame you ... and your ego must let go of any desire to be seen as important in a world filled with pathetic, needy cunts who need you to be their slave.
