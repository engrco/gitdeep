---
created: 2022-01-26T12:21:56-06:00
lastmodified: 2022-01-26T12:31:23-06:00
---

# Do polls matter?  Probably not ... polls are just a way of calibration of one's personal sentiments ... but meta-polls and markets are far more important than ANY news source.

https://projects.fivethirtyeight.com/biden-approval-rating/

If you are still following news sources, you shouldn't ... you should pay more attention to the networks and communities that really matter to you professionally [in terms of the macro view], collegially in terms of the micro view of your personal professional colleagues] and personally [in terms of family, friends]...  and you should always be trying to upgrade yourself and the people around you ... sometimes, it's important to leave the people who are stuck in the past, following old news behind without giving up on them.

Don't look in the rear-view mirror...unless you are senile, you are not going that way.
