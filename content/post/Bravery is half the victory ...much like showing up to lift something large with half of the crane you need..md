---
created: 2021-12-24T13:22:16-06:00
lastmodified: 2021-12-24T13:35:56-06:00
---

# Bravery is perhaps half the victory ... like trying to lift a very large object with a crane half of the size you will need to accomplish the job.

https://grimfrost.com/blogs/blog/viking-quotes
