---
title: Freedom Comes From Independence
subtitle: Discipline Requires Independence
date: 2021-11-08
tags: ["example", "bigimg"]
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

# Freedom grows our of the ability to THINK ... to read actively, discuss ideas and to form one's thoughts INDEPENDENTLY

The DISCIPLINE of an independent life and freedom requires that we we think INDEPENDENTLY, not blindly follow orders ... we cannot OWN something that we have not *PURCHASED* with an independent investment of thought and consideration ... developing the ability to think, analyze, consider, explain, debate and defend our views to others is something that educuation must do ... **education must inculcate independence.**