---
created: 2022-02-10T13:31:12-06:00
lastmodified: 2022-02-10T13:34:31-06:00
---

# Tapqir is a modular program that uses a chosen probabilistic model to interpret experimental data

https://tapqir.readthedocs.io/en/stable/tutorials/part_i.html


Multi-wavelength single-molecule fluorescence colocalization (CoSMoS) methods allow elucidation of complex biochemical reaction mechanisms.

https://www.biorxiv.org/content/10.1101/2021.09.30.462536v3
