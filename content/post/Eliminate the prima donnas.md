---
created: 2021-12-04T13:37:18-06:00
---

# Eliminate the prima donnas

Of course it's necessary to recognize contributions and teamwork ... but teams will do that ....it is necessary to weed out the prima donnas and/or entice them to leave for a better offer ... you want the legit 10X-er ... the person who makes everyone around them so much better that the contribution is 10X of the normal person --- but that's impossible if a person is claiming to have been a 10X-er and not cheering the people who are improving their game.
