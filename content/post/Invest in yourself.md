---
created: 2022-01-21T12:00:24-06:00
lastmodified: 2022-01-21T12:12:00-06:00
---

# Invest in yourself

Turn your back on the world and work on your own things OR fully embrace everything that the world has to off as you reach out to contribute and build your network OR better yet, diversify your investments and do some of both.

Just be sure that you are doublecheck your assumptions and making your allocation rules based on where you want to go ... your investments need to reflect who YOU want to become ... and not driven by what you are being sold or what you imagine will be valuable ... anyone who tells you that they know what will be valuable is probably lying to you, unless they have insights about who you are ... unless they counsel investing in you, they might just be seeking to unload their junk.
