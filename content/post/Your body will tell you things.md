---
created: 2022-01-30T07:35:40-06:00
lastmodified: 2022-01-30T08:20:14-06:00
---

# Your body will tell you things

If you listen ... it can't tell you anything if you are numb, drunk, stoned or high ... if you're having a psychedelic experience, that may open the doors of perception to other intellectual experiences but it will interfere with your ability to perceive what else your body, mind and soul are telling you.

Control the projector ... JEALOUSLY guard and control what is going into your body, mind and soul ... what you eat is OBVIOUS enough -- you are an adult, ie you can manage your own poisons -- it's one thing to actively, attentively taste and fully experience something like a bite of a donut without turning into a pathetic couch-potato lardass -- but if you passively just swallow donuts every time you see one, you will be in trouble ... particularly because you will habitually and PHYSIOLOGICALLY need another donut ... just like a crack addict, but a particularly devious crack addict who will find ways to rationalize why it's okay for a respectable professional to enjoy baked goods.

Food should be obvious ... but it also should be obvious that that can't just passively swallow advertisements or any kind of programming or or movie plot ... again, it's one thing to ACTIVELY evaluate the taste of content ... it's another thing entirely when you have "your favorite programs" ....when every day or maybe every Sunday afternoon, you get your plate of snacks and zone out to PASSIVELY swallow your content, your favorite programming along with all of the advertising POISON that comes along for the ride ... so that you program yourself to feel inadequate because you can't afford that food, that vacation, that automobile or, as you age, that healthcare plan or medications or orthopedic shoes which you program yourself to feel entitled to.

Your body, mind and soul will tell you things if you tune in to your spiritual center ... to that core spark of your existence from your Creator ... that core spark is NOT your ego ... people who ego driven are not driven by the spark of Life from their Creator ... ego is like flame ... like a ridiculous flaming faggot ... like those embers from a fire that glow bright, but then very quickly are DONE and just ash.  You need to find that spark from your Creator and to keep focused upon it .. that spark is all about knowing your WHY ... your WHY will not be sustainable if it is ego-driven ... 

The spark from your Creator will ignite whatever little you have to work with ... you should not be like the flaming faggot ... you will be much better off if you are smoldering and ready to burst into flame when the moment presents itself.
