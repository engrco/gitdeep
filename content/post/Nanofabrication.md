---
created: 2022-02-04T13:59:47-06:00
lastmodified: 2022-02-04T14:17:55-06:00
---

# Nanofabrication

250,000 PRECISELY-controlled microshutters for super-granular hyperspectral spectrography ... still almost belief ... there might be reason to believe that the human species can figure out how not to exterminate itself.

There still hope in Pandora's little box ... a very small nanoscale dose of hope.

https://jwst-docs.stsci.edu/jwst-near-infrared-spectrograph/nirspec-instrumentation/nirspec-micro-shutter-assembly
