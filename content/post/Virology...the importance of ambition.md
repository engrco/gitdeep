---
created: 2021-12-02T08:13:43-06:00
lastmodified: 2021-12-02T08:17:41-06:00
---

# Virology...the importance of ambition

You have to work at improving the strength of your immune system while also reducing exposure to various vectors of viral infection ... and you need to be ambitious about informing yourself so that you spend more time enjoying life while getting healthier and healthier.
