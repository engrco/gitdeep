---
created: 2021-12-13T05:54:19-06:00
lastmodified: 2021-12-13T05:59:23-06:00
---

# If you want it, you will need to pay for it?

If you want to be independent, you will need to earn your independence ... no one can really give you your independence or give you your freedom ... humans have a natural predisposition to dependency and addiction ... if you want to be free of addiction and dependency, you will need to earn it.
