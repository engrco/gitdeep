---
created: 2022-01-20T19:59:18-06:00
lastmodified: 2022-01-20T20:07:18-06:00
---

# Enclosed porch or overwintering greenhouse

Could a drafty, but enclosed porch kept at 50 degrees  be used as the location of unvented natgas burners used to heat water for a partial hydronic system?  Separate system or just a change of location for water heater?

* reducing overall heating costs
*  greenhouse to overwinter perennials
*  reduced furnace cycles / op cost
*  extending life of furnace.
