---
created: 2022-01-08T13:30:21-06:00
lastmodified: 2022-01-08T13:34:54-06:00
---

# Power always corrupts and destroys itself

It takes sycophants and idiots too stupid to pay attention along with it ... and there is additional collateral damage when power and affluence inevitably self-destruct.

The ONLY source of wealth that matters is ability to adapt and thrive in unpredictable uncertainty ... there is no hedge ... there's only nasty, messy toughness.
