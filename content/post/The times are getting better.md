---
created: 2021-12-20T08:24:51-06:00
lastmodified: 2021-12-20T15:55:36-06:00
---

# The times are getting better

At 60 I find that life has never been easier ... much of the reason for that is that I no longer feel any need to put up with any complainers EVER ... long ago, I realized that complainers and whiners are just going to work at making others miserable ... I wake up and and I can do whatever I want to do, such as training and exercise, without worrying one iota about the delusional imbeciles who still need their drugs or pills or comforts and conveniences to get by.

By 50 I had retired, but I still imagined that I had things to do in the world of philanthropy ... so I learned about venture philanthropy by developing ways to fund my causes...so I learned that ANY cause can either be self-funding OR it can work at more effectively eliminating the ROOT cause of the so-called cause...this means that during my 50s what I became really, absolutely, totally convinced of is that philanthropy is about feeding the fundraisers who principally work on stirring up guilt in order to get fools to part with their money in order to fund perpetual fundraising organizations.

During my 40s I learned that there are very, very, VERY few companies that are producing products that make people legitimately better off over time ... AND I learned why this will STAY so, the best of the best in terms of corporate leadership are still completely driven by a delusional fascination in what is CURRENTLY politically popular, ie greenwashing is only one example of this.

During my 30s, I learned that people will REFUSE to invest in things that will radically transform the nature of commerce ... when people cannot begin to conceive of something truly transformative in a larger sense, they will invest in INSANELY stupid things that they imagine other people want.

During my 20s, I threw mud at the wall to see what would stick in the realm of entrepreneurial and research activities ... and generally realized why most humans are DEEPLY pathetic materialists and fanatical believers in conformity, especially in conformity to the human fascination with material possessions and human measures of wealth.

From the time that I was 10 until I was 20, I learned about business and the process of self-education or learning new challenging tools, technologies, skills ... how to AI a cow, how to select for traits and do genetic improvement in plants and animals, how formulate least-cost balanced rations and set up herd health programs, how to overhaul an engine, how to wire a shop, how to frame up construction and put up sheet rock, how to troubleshoot a mechanical contraption like a hay baler, how to set up and run a brake press ... MOSTLY, I learned that that regardless of the issue -- EVENTUALLY I would figure it out ... it's not about mastery of a microskill, it's about mastery of the macroskill of SELF education.

My first 10 years ... I learned why bullies should never be respected AND why they will not learn ... gossips and tattletales cannot be fixed -- their punishment is that they have to live in their own lives, the one that they destroy -- even though they will not knock off the pathetic attempts to destroy others ... on a vaguely related matter [because of how education is necessarily an intensely personal and introspective effort, and never something one is given], I learned why the very last place that anyone learns anything is in the classroom.
