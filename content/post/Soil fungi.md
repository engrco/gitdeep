---
created: 2021-12-13T13:44:43-06:00
lastmodified: 2021-12-13T13:59:22-06:00
---

# Soil fungi meta-community hubs

Mortierella (Mortierellales), Cladophialophora (Chaetothyriales), Ilyonectria (Hypocreales), Pezicula (Helotiales), and Cadophora (incertae sedis) had broad geographic and host ranges across the northern (cool-temperate) region, while Saitozyma/Cryptococcus (Tremellales/Trichosporonales) and Mortierella as well as some arbuscular mycorrhizal fungi were placed at the central positions of the metacommunity-scale network representing warm-temperate and subtropical forests in southern Japan.

Start by reviewing a basic prerequisite background in the biochemistry of saprotropic nutrition ... try to *think* like a fungal digestive system

https://en.wikipedia.org/wiki/Saprotrophic_nutrition and https://en.wikipedia.org/wiki/Lysis


https://en.wikipedia.org/wiki/Mortierella

https://en.wikipedia.org/wiki/Cladophialophora

https://en.wikipedia.org/wiki/Hypocreales

https://en.wikipedia.org/wiki/Pezicula

https://en.wikipedia.org/wiki/Helotiales
