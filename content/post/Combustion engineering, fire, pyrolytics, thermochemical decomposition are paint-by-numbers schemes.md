---
created: 2021-12-11T05:32:46-06:00
lastmodified: 2021-12-11T05:38:11-06:00
---

# Combustion engineering, fire, pyrolytics, thermochemical decomposition are paint-by-numbers schemes

Battery technology is not that far behind ... neither is something like the manipulation of hearing/sound or light/vision.

Biological processes, living organisms and even non-living things like viruses are much more poorly understood ... still in the realm of art ... as are things like olfactory, taste and preference, emotion, addiction.
