---
created: 2021-11-13T23:51:30-06:00
lastmodified: 2021-11-13T23:59:23-06:00
---

# Just the data, not the metadata

Do your best to live in accordance with what you can discern to be your Creator's will ... if your best efforts glorifies your Creator, let the chips fall where they may.

Ditch your ego and stop worrying about what you will be remembered for ... worry about the data, the metadata will take care of themselves ... just stay at it.
