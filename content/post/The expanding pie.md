---
created: 2022-01-10T02:33:01-06:00
lastmodified: 2022-01-10T02:33:18-06:00
---

# The expanding pie

I am a game theorist ... I like games, but I don't care for zero-sum games like crypto which bamboozle idiots into imagining that a currency creates intrinsic value.

Zero sum means that for every winner, someone else has to lose.

I prefer games in which it's not just about dividing the pie ... but the point of the game is generally about figuring out how to grow or expand the pie.
