---
created: 2022-01-29T19:36:38-06:00
lastmodified: 2022-01-29T19:48:05-06:00
---

# Meditation/Lead The Qi

We are grateful for now and for every one of the breaths we are granted ... we are blessed today beyond whatever we could have imagined yesterday, last week, a month or year ago ... life continues to improve as we seek being re-united with our Creator, our Father in heaven.

We bear no grudges and seek to go forward without baggage ...we forgive all, including ourselves ... allowing God to take over all responsibility for vengeance and punishment. We don't need to concern ourselves with punishing anyone, even ourselves.

We will tty to improve each moment, using what we are given to greater advantage while praying not be lead into temptation
