---
created: 2022-01-17T15:33:53-06:00
lastmodified: 2022-01-17T15:46:22-06:00
---

# Your problem, like the love of your life, will find you.

You might be rather promiscuous when it comes to having different flings or temporary loves of your life ... a temporary fling might indeed be the love of one's life ... but we change, move from one lifetime or worldview to another. 

Some are more mercurial than others ... whereas weak people demand that others never change so that their faux safe space never changes.

There's no harm in exploration when it's clear you are exploring ... the important thing is to be brutally honest and frank with yourself and anyone who is counting on you, to appropriately signal that you are not their bitch or their slave ... you should never hurt people just for the sake of hurting them, but the very problems or pursuits which compel you to be interested in them should not be denied for the sake of appearing to give fidelity to something which you cannot really give fidelity to.
