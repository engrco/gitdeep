---
created: 2021-11-12T19:18:16-06:00
modified: 2021-11-12T20:04:54-06:00
---

# Social  Capital / Public Shared Goods

Non-rivalrous, non-excludable public goods or even club goods in which the benefit improves via network affects generate group capital ... group capital and virtual groups allow people to tailor crowdliness to participate in ways that allow them to cherry-pick the attributes of the highest benefit to them.

People who are programmed by a world driven by scarcity economics simply cannot comprehend the world of social capital ... the fundamental assumption of scarcity economics is completely invalid and wrong in the world of group capital and non-excludable knowledge.

The virtual realm enables the creation of sharable open source knowledge ... even as the controllers of assets still mentally stuck in the realm of scarcity economics will refuse to participate and even work to undermine the realm of group capital ...this new kind of productive knowledge asset completely transcends, revolutionizes and disrupts the production possibility curve that FORMERLY bounded the set of products possible.
