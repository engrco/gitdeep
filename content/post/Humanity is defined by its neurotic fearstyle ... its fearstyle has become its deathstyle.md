---
created: 2022-01-31T03:09:24-06:00
lastmodified: 2022-01-31T03:21:10-06:00
---

# Humanity is defined by its neurotic fearstyle ... its fearstyle has become its deathstyle

Human lifestyles are pathetic and perverse ... increasingly automating the increase in entropy ... of course, this increase in entropy will exterminate the species well before it has a chance to really damage the Universe.

The human lifestyle is not about life, it's about fear ... human existence has become a self-predatory fearstyle ...the human fearstyle is especially pathetic and increasingly weak in the way that it progressively, continually and half-intelligently makes itself more and more perversely worried ... as it gets more neurotically worried, demanding and censorious ... clamoring for greater cleanliness and scared shitless of the ecosystems that are alive and too complicated for a declining collective consciousness to understand.

What can possibly be done to help a species that responds to crisis by emptying storeshelves of toilet paper because members of the species are completely terrorized and wigged out by their own shit ... this is a species that will lay down and die before it digs its latrines or comes to terms with dealing with its own fearstyle ... so the species is in an accelerating death spiral, a psychotic, neurotic depressing suicidal decline ... the human fearstyle is becoming its deathstyle ... the vaccine you love is only just one of the tools the species will use for its own self-inflicted demise.
