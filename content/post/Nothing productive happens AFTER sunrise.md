---
created: 2022-02-09T05:04:56-06:00
lastmodified: 2022-02-09T05:06:14-06:00
---

# Nothing productive happens AFTER sunrise

Most of the productive work is done or planned and set in motion before sunrise.
