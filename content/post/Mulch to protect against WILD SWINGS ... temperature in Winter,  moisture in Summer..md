---
created: 2021-12-25T06:15:18-06:00
lastmodified: 2021-12-25T06:17:24-06:00
---

# Mulch to protect against WILD SWINGS ... temperature in Winter,  moisture in Summer.

https://kenosha.extension.wisc.edu/2020/12/12/winter-mulch-your-landscape-plants-2/
