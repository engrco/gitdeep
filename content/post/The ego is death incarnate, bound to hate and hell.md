---
created: 2021-12-13T23:50:52-06:00
lastmodified: 2021-12-13T23:55:37-06:00
---

# The ego is death incarnate, bound to hate and hell

The only way to eternal life is rejection of the ego and acceptance of the saving grace of the Creator's faultless son ... acceptance is the prerequisite to the saving grace being offered ... you do not have another option than renunciation of the ego, except to descend into the depths of depravity and despair.
