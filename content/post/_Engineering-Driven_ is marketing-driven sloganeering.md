---
created: 2022-01-20T12:56:59-06:00
lastmodified: 2022-01-20T13:25:46-06:00
---

# "Engineering-Driven" is marketing-driven sloganeering

Just SHOOT me!  Just effing shoot me.  But STOP making me enjoy life as I am supposed to enjoy life.

The only legit way to really do engineering is to retire and DO ENGINEERING ... as a successful artist DOES art, ie you get up in the morning and you WORK on what you want to WORK on -- but you are both SERIOUS and PASSIONATE about it ... because your work as an engineer RELAXES you.

Why do you want to DO REAL ENGINEERING ... maybe you don't need a reason; maybe you just know ...  maybe your work is for future generations ... maybe it's something that is important for the surival of the species, eg engineering space exploration or instrumentation for particle physics or biosystems engineering for healthier lifestyles OR doing or for nothing but the LOVE of just doing engineering and solving problems, possibly with friends who share the or possibly alone ... like successful entertainers or politicians who retire to read literature, become better at chess or do mathematics or do art, ie JUST for the pure love of getting totally, completely lost in one's pure love ... retirement is about finally realizing that the only thing that is relevant is your remaining supply of Life ... actually being "driven by engineering" is about complete not caring anymore about the relevance of what you're doing or the need to impress anyone.

All other engineering, in either the for-profit, academic or non-profit institutional sense, is almost necessarily going to depend upon securing more funding ... for the institution ... which inevitably comes down to marketing and marketing for the sake of marketing OR fundraising for the sake of sustaining and building a stronging fundraising competency ... engineering in all serious companies and institutions is going to be about doing those things well [and better than anyone else on the planet] that help better chase the dollars.

We should stop kidding ourselves OR ever apologizing for being marketing-driven ... marketing is about focusing upon "the customer" ... with " the customer" being the source of funding, that exemplary model citizen making up the population [who we market to] who will pays our bills because we offer greater excitement per buck than any alternative.


https://twitter.com/mipsytipsy/status/1484088516317364224?t=k7wpMaD51TwHL3aJPByQSQ&s=19
