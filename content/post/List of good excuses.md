---
created: 2022-02-06T09:29:06-06:00
lastmodified: 2022-02-06T18:17:58-06:00
---

# List of airtight excuses with multiple airtight defenses

1) I am planning to wash my cat. 
    You don't have a cat.
        I have been too busy planning to get one.

https://catalog.commamusic.com/#!explorer?sim=4102481&styles%5B%5D=latin

2) I have covid.
    You never get sick.
          That proves I am dangerously infectious.

3) I am working on my list of excuses.
     You don't need more excuses.
           You're never satisfied with my excuses.

4) I am immune to covid.
     You can't be; you aren't even vaccinated.
            I had covid so I have natural immunity.
