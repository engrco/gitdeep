---
created: 2021-11-06T15:49:12-05:00
modified: 2021-11-06T15:50:21-05:00
---

# The Tells in Our Words

Pronoun usage tells how we think about our world.
