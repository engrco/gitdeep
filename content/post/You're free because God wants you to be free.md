---
created: 2021-12-22T09:46:06-06:00
lastmodified: 2021-12-23T20:01:43-06:00
---

# You're free because God wants you to be free

The degree to which you exercise what freedom you have OR how you exercise that freedom is up to you ... that's what freedom is. You are free because you were created to be free.

Freedom doesn't come with a debt ... if some asshole tries to lay some guilt trip on you, you should politely remind that preachy fuck to fuck the fuck off ... if that doesn't work, you might have to kill them or do something that ensures the ending of the relationship.

You're free because God wants you to be free ... remember that ... but it's up to you how you express that gratitude...God isn't going to stop being God if you're ungrateful.

Gratitude gives you back far more in psychic rewards than what it costs you to express ... you basically get a choice that looks something like the following...you can celebrate each day as the greatest day ever ... OR you can lose your opportunity to appreciate and optimize that day.
