---
created: 2021-11-11T20:57:25-06:00
modified: 2021-11-11T21:27:38-06:00
---

# GitLab Jupyter

https://gitlab.com/gitlab-org/gitlab/-/issues/343024

We have implemented a better viewer for Jupyter Notebooks with #341825 (closed), but it can still be improved with a Rich Diff Viewer. Some minimum additions would be

Render embedded images
Render Math
Render whether a cell was added, changed, kept or removed
Code suggestions (Disabled in #345075 (closed)


https://gitlab.com/gitlab-org/incubation-engineering/mlops/ipynb-test-projects/diff-sample/-/commit/542d42a0196e2e94a0d5793acdee5ba997ab12bf

In other words ... the gamechanging part of this is that discussions on diffs for Jupyter Notebooks are now possible!


https://gitlab.com/gitlab-org/incubation-engineering/mlops/rb-ipynbdiff
