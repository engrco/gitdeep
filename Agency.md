---
created: 2022-03-21T07:05:53-05:00
modified: 2022-03-21T07:15:00-05:00
---

# Agency

The individual, in an ideal sense, has agency. A practical real-world individual may not actually have agency ... the tendency to cede agency or abdicate responsibilities to be informed and independent are why propagandists and advertisers work as hard as they do.

A legitimate non-corrupt democracy can democratically elect leaders and have a group agency.

Corrupt governments do not exactly have agency, except in an illegitimate, usurped sense ... citizens in corrupt governments are not always there by choice and they might not have good indications of what their options really are 
... but the corrupt government is no indication of what those citizens would democratically choose.
