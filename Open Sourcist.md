---
created: 2022-03-25T19:36:13-05:00
modified: 2022-03-27T10:04:08-05:00
---

# Open Sourcist

ONE HERO must start the project AND catalyze the community ... the community is what eventually replaces the hero.

But, it is ESSENTIAL that the ONE HERO must start the project AND catalyze the community ... there will be no community without the hero.
