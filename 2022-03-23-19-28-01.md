---
created: 2022-03-23T19:28:01-05:00
modified: 2022-03-24T21:02:06-05:00
---

# 86400.guru

One needs to experience a rapidly declining health down spiral culminating in a certain death and the priest coming in to administer last rites ... but it might take something like that for people to grasp gratitude a certain level.

But, in fact, I do honestly believe that gratitude is something that one cannot learn except through suffering and going past the point of complete despair and capitulation into a state of humility and acceptance of one's powerlessness. That's about what it takes 
... or otherwise, you just don't really get it yet.

I truly hope that by somehow going through the motions of working at being able to be more ready ... in EACH of the 86,400 seconds of each day that I can be more grateful for every second.

I do honestly BELIEVE that real gratitude or true humility are not things that one can learn except through suffering.

The only way to BUILD upon that foundation is to RIGOROUSLY train in improving the discipline ... of at least TRYING, each day, to go through the motions of working at being able to be more ready and more grateful for every second.

REAL gratitude, by one's actions managing time, is the ONLY thing that that one can do to show love to one's Creator ....because time is ALL that we have ... and time affects the stewardship and prioritization of the efforts to manage all other assets. Most material assets should be given away ... because of how the baggage of material assets affects time management. 

Cleverness ... especially in thwarting someone else's time use is particularly EVIL ... the sort of thing that is punishable by torture and death.
